import Home from './HomeScreen';
import Login from './auth/LoginScreen';
import Logout from './auth/LogoutScreen';
import Personenauto from './regelgeving/PersonenautoScreen';
import EditExam from './EditExamScreen';
import Profile from './ProfileScreen';
import ShowAllExams from './ShowAllExams';
import Messages from "./messagesScreen";
import ExamsResult from "./ExamsResult";
import RegelgevingShow from "./regelgeving/RegelgevingShowScreen";
import RegelgevingSide from "./regelgeving/RegelgevingSideScreen";
import DrawerContent from "./DrawerContent"

// Practice exam screens imports
import CreatePracticeExam from './practiceExam/createScreen';
import CasePracticeExam from "./practiceExam/caseScreen";
import PracticeQuestions from "./practiceExam/practiceQuestionsScreen";
import FinishPracticeExam from "./practiceExam/finishScreen";
import PracticeExamResult from "./practiceExam/resultScreen"
import ShowPracticeExam from "./practiceExam/showScreen";
import AddExamStepOne from './addExam/AddExamStepOne';
import AddExamStepTwo from './addExam/AddExamStepTwo';
import AddExamStepThree from './addExam/AddExamStepThree';
import AddExamStepFour from './addExam/AddExamStepFour';

export {
    Home,
    AddExamStepOne,
    AddExamStepTwo,
    AddExamStepThree,
    AddExamStepFour,
    Login,
    Logout,
    Personenauto,
    EditExam,
    RegelgevingSide,
    Profile,
    ShowAllExams,
    Messages,
    RegelgevingShow,
    ExamsResult,
    DrawerContent,

    // Practice exam screens export
    CreatePracticeExam,
    CasePracticeExam,
    PracticeQuestions,
    FinishPracticeExam,
    PracticeExamResult,
    ShowPracticeExam
}
