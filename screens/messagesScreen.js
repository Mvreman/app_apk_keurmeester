import React from 'react';
import { connect } from 'react-redux';
import { fetchMessages } from "../redux/actions/messageAction";
import {FlatList, Text, View} from 'react-native';
import { Style } from "../source";
import {Collapse, CollapseBody, CollapseHeader} from "accordion-collapse-react-native";
import Dash from "react-native-dash";
import {Ionicons} from "@expo/vector-icons";

/**
 *
 * @param messages
 * @returns {*}
 * @constructor
 */
const Messages = ({ messages }) => {
    return (
        <View>
            <View style={Style.Messages.messageCard}>
                <Collapse style={[Style.General.card, Style.Messages.messageCardInner]} >
                    <CollapseHeader>
                        <View>
                            <View>
                                <View>
                                    <Text style={{ fontWeight: 'bold'}}>{messages.title}</Text>
                                    <Ionicons name="ios-arrow-down" size={24} color="black" style={Style.Messages.messageArrow} />
                                </View>
                            </View>
                        </View>
                    </CollapseHeader>
                    <CollapseBody>
                        <Dash dashGap={0} style={Style.ShowAllExams.dashHorizontal}/>
                        <Text>{messages.text}</Text>
                    </CollapseBody>
                </Collapse>
            </View>
        </View>
    )
}

class HomeMessageComponent extends React.Component {
    /**
     * Function for the update api call of messages
     *
     * @returns {Promise<void>}
     */
    componentDidMount() {
        this.props.fetchMessages(this.props.login);
    }

    /**
     * Dynamic render function for messages
     *
     * @returns {*}
     */
    render() {
        return (
            <View style={{flex: 1}}>
                <View style={Style.General.container}>
                {this.props.failed === true ?
                    <Text style={{backgroundColor: '#ff4d4d', color: '#ffffff', height: 20, textAlign: "center"}}>Error:
                        {this.props.error.message}</Text> : null}
                    <Text style={Style.General.h2}>Berichten box</Text>
                    {/*Flatlist that renders the const: "Message" in it*/}
                    <FlatList
                        style={Style.General.margin0}
                        scrollEnabled={true}
                        keyExtractor={(item, index) => index.toString()}
                        data={this.props.messages}
                        renderItem={({item}) => <Messages messages={item}/>}
                    />
                </View>
                <View style={ Style.General.footer }/>
            </View>
        )
    }
}

/**
 *
 * @param state
 * @returns {{messages: (string|string|TestMessage[]|undefined|[]), failed: *, error: *, login: []}}
 */
const mapStateToProps = (state) => {
    return {
        error: state.messageReducer.error,
        failed: state.messageReducer.failed,
        messages: state.messageReducer.messages,
        login: state.loginReducer.login,
    }
};

/**
 *
 * @type {{fetchMessages: (function(): function(...[*]=))}}
 */
const mapDispatchToProps = {
    fetchMessages: fetchMessages,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeMessageComponent);
