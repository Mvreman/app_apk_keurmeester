import React from 'react';
import { View, Text, TextInput, TouchableOpacity } from 'react-native';
import { CheckBox } from 'react-native-elements'
import { Formik } from 'formik';
import DropDownPicker from 'react-native-dropdown-picker';
import {LinearGradient} from "expo-linear-gradient";
import { connect } from 'react-redux';
import * as yup from 'yup';
import practiceExamStyle from "../../source/styles/practiceExamStyle";
import { Style } from "../../source/index";

// Yup validation.
const reviewSchema = yup.object({
    Email: yup.string().email(),
    Informer: yup.bool(),
    Ontvanger: yup.string(),
});

var IndexValue;

class AddExamStepThree extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            Ontvanger: "Werkgever",
        }
        this.baseState = this.state;
    }

    // Set item when item gets changed in drop down.
    changeExamReciever = (item) => {
        this.setState({
            Ontvanger: item.value
        })
    }

    // Redirect to the previous screen.
    previous = () => {
        this.props.navigation.reset({
            index: 0,
            routes: [{ name: 'BookExamStep02' }],
        })
    }

    // Redirect to the next screen
    next = () => {
        this.props.navigation.reset({
            index: 0,
            routes: [{ name: 'BookExamStep04' }],
        })
    }

    render() {
        return (
            <View style={{backgroundColor: '#fff' , flex: 1}}>
                <LinearGradient colors={['#F2F2F2', 'transparent']}
                                style={Style.General.gradient}/>
                <View style={Style.General.container}>
                    <Text style={Style.General.h1}>Nieuw examen boeken</Text>

                    <Formik
                        initialValues={{
                            Type: this.props.bookExam.exam_type,
                            Datum: this.props.bookExam.date,
                            Locatie: this.props.bookExam.city,
                            Email: "",
                            Informer: false,
                            Ontvanger: this.state.Ontvanger,
                        }}
                        validationSchema={reviewSchema}
                        onSubmit={(values) => {
                            console.log(values)
                        }}>
                        {({ setFieldValue, handleSubmit, handleChange, errors, values }) => (
                            <View>
                                <Text style={Style.ThirdExame.bold}>Type Examen:</Text>
                                <TextInput
                                    style={Style.ThirdExame.input}
                                    value={values.Type}
                                    editable={false}
                                    selectTextOnFocus={false}
                                />

                                <Text style={Style.ThirdExame.bold}>Examendatum:</Text>
                                <TextInput
                                    style={Style.ThirdExame.input}
                                    value={values.Datum}
                                    editable={false}
                                    selectTextOnFocus={false}
                                />

                                <Text style={Style.ThirdExame.bold}>Locatie:</Text>
                                <TextInput
                                    style={Style.ThirdExame.input}
                                    value={values.Locatie}
                                    editable={false}
                                    selectTextOnFocus={false}
                                />

                                <Text style={Style.ThirdExame.bold}>Uitnodigen via email:</Text>
                                <TextInput
                                    style={Style.ThirdExame.input}
                                    value={values.Email}
                                    onChangeText={handleChange('Email')}
                                />
                                <Text style={Style.ThirdExame.errorDisplay}>{errors.Email}</Text>

                                <Text style={Style.ThirdExame.bold}>Werkgever informeren:</Text>
                                <CheckBox
                                    title='Ja. ik wil graag dat mijn werkgever geïnformeerd word.'
                                    checked={values.Informer}
                                    onPress={() => setFieldValue('Informer', !values.Informer)}
                                />
                                <Text style={Style.ThirdExame.errorDisplay}>{errors.Informer}</Text>

                                <Text style={Style.ThirdExame.bold}>Ontvanger waardedocument:</Text>
                                <DropDownPicker
                                    items={[
                                        { label: 'Deelnemer', value: 'Deelnemer' },
                                        { label: 'Werkgever', value: 'Werkgever' },
                                    ]}
                                    defaultIndex={IndexValue}
                                    containerStyle={{ height: 40 }}
                                    onChangeItem={item => {
                                        this.changeExamReciever(item);
                                        setFieldValue('Ontvanger', item.value);
                                    }}
                                />
                                <Text style={Style.ThirdExame.errorDisplay}>{errors.Ontvanger}</Text>

                            </View>
                        )}

                    </Formik>
                </View>
                <View style={practiceExamStyle.bottomView}>
                    <TouchableOpacity style={practiceExamStyle.btnPrevious} onPress={this.previous}>
                        <Text>Vorige</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={practiceExamStyle.btnNext} onPress={this.next}>
                        <Text>Volgende</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export const resetStateStepThree = AddExamStepThree.reset = () => {
    try {
        new AddExamStepThree(() => {
            this.setState(this.baseState);
        })
    } catch (e) {
        console.log(e);
    }
}

// Set the map state
const mapStateToProps = (state) => {
    return {
        bookExam: state.examsReducer.bookExam,
    }
};

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(AddExamStepThree);
