import React from 'react';
import { connect } from 'react-redux';
import { TextInput, View, Text, TouchableOpacity } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import * as yup from 'yup';
import { Formik } from 'formik';
import { LinearGradient } from "expo-linear-gradient";
import { Style } from '../../source';
import { bookExam } from '../../redux/actions/bookExamAction';
import EXAMTYPES from '../../constants/EXAMTYPES';
import BRANCH from '../../constants/BRANCH';
import LOCATIONS from '../../constants/LOCATIONS';
import practiceExamStyle from "../../source/styles/practiceExamStyle";

class AddExamStepOne extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            branche: null,
            branches: BRANCH.DROPDOWN,
            examType: null,
            examTypes: [],
        }
        this.baseState = this.state;
    }


    /**
     * function that changes the exam types available in the dropdown when the selected branch dropdown gets changed
     *
     * @param {string} item
     */
    changeExamBranch = (item) => {
        let examTypes = [];

        switch (item.value) {
            case BRANCH.PERSONENAUTOS:
                examTypes = EXAMTYPES.lightweight
                break;
            case BRANCH.BEDRIJFAUTOS_LICHT:
                examTypes = EXAMTYPES.lightweight
                break;
            case BRANCH.BEDRIJFSAUTOS_ZWAAR:
                examTypes = EXAMTYPES.heavyweight
                break;
            case BRANCH.DRIEWIELIGE_MOTORRIJTUIGEN:
                examTypes = EXAMTYPES.lightweight
                break;
            case BRANCH.AANHANGWAGENS:
                examTypes = EXAMTYPES.heavyweight
                break;
            case BRANCH.OVERIGE_REGELGEVING:
                examTypes = EXAMTYPES.lightweight
                break;
            default:
                examTypes = []
        }

        this.setState({
            examTypes,
            examType: null,
            branche: true
        });
    }

    /**
     * When examtype gets changed to a string, the value in the dropdown won't be defaultnull anymore
     *
     * @param {string} item
     */
    changeExamType = (item) => {
        this.setState({
            examType: item.value
        });
    }

    previous = () => {
        this.props.navigation.goBack()
    }

    render() {
        return (
            <View style={{ backgroundColor: '#fff', flex: 2 }}>
                <LinearGradient colors={['#F2F2F2', 'transparent']}
                    style={Style.General.gradient} />
                <Formik
                    initialValues={
                        {
                            branch: this.props.bookExam.branch,
                            exam_type: this.props.bookExam.exam_type,
                            gba: '',
                            city: this.props.bookExam.city ? this.props.bookExam.city : 'Nieuwegein',
                            state: 'planned'
                        }
                    }
                    validationSchema={
                        yup.object().shape({
                            branch: yup
                                .string()
                                .required('Branch is verplicht!'),
                            exam_type: yup
                                .string()
                                .required('Examen type is verplicht!'),
                            gba: yup
                                .number()
                                .required('BSN is verplicht!')
                                .test(
                                    'gba-match',
                                    'BSN komt niet overeen',
                                    value => {
                                        return value === parseInt(this.props.user.user_details.ssn)
                                    }
                                ),
                            city: yup
                                .string()
                                .required('Locatie is verplicht!'),
                        })
                    }
                    onSubmit={values => {
                        delete values.gba;
                        this.props.bookExam(values);
                        this.props.navigation.reset({
                            index: 0,
                            routes: [{ name: 'BookExamStep02' }],
                        })
                    }}>
                    {({ setFieldValue, handleSubmit, handleChange, isValidating, isSubmitting, errors, values }) => (
                        <View style={{ backgroundColor: '#fff', flex: 1.2 }} >
                            <LinearGradient colors={['#F2F2F2', 'transparent']} style={Style.General.gradient} />
                            <View style={Style.General.container}>
                                <Text style={Style.General.h1}>Nieuw examen boeken</Text>
                                <Text style={[Style.General.h6, Style.Profile.formFieldLabel]}>Kies een branche:</Text>
                                <DropDownPicker
                                    items={this.state.branches}
                                    defaultNull={this.state.branche === null}
                                    placeholder="Selecteer een branche"
                                    onChangeItem={item => {
                                        this.changeExamBranch(item);
                                        setFieldValue('branch', item.value);
                                    }}
                                    containerStyle={{ width: "100%", height: 40, marginBottom: 10 }}
                                    zIndex={9999}
                                />
                                {errors.branch &&
                                    <Text style={{ fontSize: 10, color: 'red' }}>{errors.branch}</Text>
                                }

                                <Text style={[Style.General.h6, Style.Profile.formFieldLabel]}>Type examen:</Text>
                                <DropDownPicker
                                    items={this.state.examTypes}
                                    defaultNull={this.state.examType === null}
                                    placeholder="Selecteer een type"
                                    onChangeItem={item => {
                                        this.changeExamType(item);
                                        setFieldValue('exam_type', item.value);
                                    }}
                                    containerStyle={{ width: "100%", height: 40, marginBottom: 10 }}
                                    zIndex={9998}
                                />
                                {errors.exam_type &&
                                    <Text style={{ fontSize: 10, color: 'red' }}>{errors.exam_type}</Text>
                                }
                                <Text style={[Style.General.h6, Style.Profile.formFieldLabel]}>GBA controle:</Text>
                                <TextInput
                                    style={Style.Exames.dateInput}
                                    keyboardType={'numeric'}
                                    defaultNull
                                    placeholder="BSN: 75348998"
                                    onChangeText={handleChange('gba')}
                                    value={values.gba}
                                />
                                {errors.gba &&
                                    <Text style={{ fontSize: 10, color: 'red' }}>{errors.gba}</Text>
                                }

                                <Text style={[Style.General.h6, Style.Profile.formFieldLabel]}>Locatie:</Text>
                                <DropDownPicker
                                    items={LOCATIONS.DROPDOWN}
                                    value={this.props.bookExam.city}
                                    defaultValue={this.props.bookExam.city}
                                    placeholder="Selecteer een locatie"
                                    onChangeItem={item => {
                                        setFieldValue('city', item.value);
                                    }}
                                    containerStyle={{ width: "100%", height: 40, marginBottom: 10 }}
                                    zIndex={9999}
                                />
                                {errors.city &&
                                    <Text style={{ fontSize: 10, color: 'red' }}>{errors.city}</Text>
                                }
                            </View>

                            <View style={[practiceExamStyle.bottomView, { flex: 0.1 }]}>
                                <TouchableOpacity style={practiceExamStyle.btnPrevious} onPress={this.previous}>
                                    <Text>Vorige</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={practiceExamStyle.btnNext} onPress={handleSubmit}>
                                    <Text>Volgende</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    )}
                </Formik>
            </View>
        )
    }
}

export const resetStateStepOne = AddExamStepOne.reset = function(){
    try {
        new AddExamStepOne(() => {
            this.setState(this.baseState);
        })
    } catch (e) {
         console.log(e);
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.loginReducer.login,
        bookExam: state.examsReducer.bookExam,
        loading: state.examsReducer.loading,
    }
};

const mapDispatchToProps = {
    bookExam,
}

export default connect(mapStateToProps, mapDispatchToProps)(AddExamStepOne);
