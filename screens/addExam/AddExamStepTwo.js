import React, { PureComponent } from 'react';
import { View, Text, Button, FlatList, TouchableOpacity } from 'react-native';
import { connect } from "react-redux";
import moment from 'moment';
import {LinearGradient} from "expo-linear-gradient";
import fetchExamDates from '../../redux/actions/examDatesAction';
import { bookExam } from '../../redux/actions/bookExamAction';
import { Style } from "../../source/index";
import practiceExamStyle from "../../source/styles/practiceExamStyle";
import BRANCH from "../../constants/BRANCH";

class AddExamStepTwo extends React.Component {
    constructor(props) {
        super(props);

        // save selected item key in store
        this.state = {
            selectedItem: null
        }
        this.baseState = this.state;
    }

    componentDidMount() {
        this.props.fetchExamDates()
    }

    // Storing data.
    handlePress = (date, listKey) => {
        this.props.bookExam(date);

        this.setState({
            selectedItem: listKey,
        })
    }

    // Redirect to the previous screen.
    previous = () => {
        this.props.navigation.reset({
            index: 0,
            routes: [{ name: 'BookExamStep01' }],
        })
    }

    // Redirect to the next screen.
    next = () => {
        this.props.navigation.reset({
            index: 0,
            routes: [{ name: 'BookExamStep03' }],
        })
    }

    render() {
        return (
            <View style={{backgroundColor: '#fff' , flex: 1}}>
                <LinearGradient colors={['#F2F2F2', 'transparent']}
                                style={Style.General.gradient}/>
                <View style={Style.General.container}>
                    <Text style={Style.General.h1}>Nieuw examen boeken</Text>

                    <Text style={Style.General.h6}>Kies een datum</Text>

                    <FlatList
                        // filling flat list with all the exam dates.
                        data={this.props.dates}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item, index }) => (<DateFlatList handlePress={this.handlePress} selectedItem={this.state.selectedItem} date={item} listKey={index} />)}
                    />
                </View>
                <View style={practiceExamStyle.bottomView}>
                    <TouchableOpacity style={practiceExamStyle.btnPrevious} onPress={this.previous}>
                        <Text>Vorige</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={practiceExamStyle.btnNext} onPress={this.next}>
                        <Text>Volgende</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )

    }
}

class DateFlatList extends PureComponent {
    render() {
        return (
            <View>
                <TouchableOpacity onPress={() => this.props.handlePress(this.props.date, this.props.listKey)}>
                    <View style={this.props.listKey === this.props.selectedItem ? Style.SecondExame.selected : Style.SecondExame.container}>
                        <View style={Style.Home.plannedExams}>
                        <View style={{ flex: 1, flexDirection: 'column', alignItems: "flex-start" }}>
                                <View style={{ flexWrap: 'wrap' }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={{ fontSize: 12 }}>Examennummer: </Text>
                                    </View>
                                    <View style={{flexDirection: 'row' }}>
                                        <Text style={{ fontSize: 12 }}>Datum: </Text>
                                    </View>
                                    <View style={{flexDirection: 'row' }}>
                                        <Text style={{ fontSize: 12 }}>Open plekken: </Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', alignItems: "flex-end" }}>
                                <View style={{ flexWrap: 'wrap' }}>
                                    <View style={{ flexDirection: 'row-reverse' }}>
                                        <Text style={{ fontSize: 12 }}>{Math.floor(100000 + Math.random() * 900000)}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row-reverse' }}>
                                        <Text style={{ fontSize: 12 }}>{moment(this.props.date.date).local().format('DD-MM-YYYY')}, {this.props.date.time}</Text>
                                    </View>
                                    <View style={{flexDirection: 'row-reverse' }}>
                                        <Text style={{ fontSize: 12 }}>4</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

export const resetStateStepTwo = AddExamStepTwo.reset = function(){
    try {
        new AddExamStepTwo(() => {
            this.setState(this.baseState);
        })
    } catch (e) {
        console.log(e);
    }
}

// Set the map state.
const mapStateToProps = (state) => {
    return {
        bookExam: state.examsReducer.bookExam,
        dates: state.fetchAllExameDates.dates,
    }
};

// Set dispatch state.
const mapDispatchToProps = {
    fetchExamDates,
    bookExam,
}

export default connect(mapStateToProps, mapDispatchToProps)(AddExamStepTwo);
