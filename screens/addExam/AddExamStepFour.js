import React from 'react';
import {View, Text, TextInput, Linking, Alert, TouchableOpacity, ScrollView} from 'react-native';
import { CheckBox } from 'react-native-elements'
import { Formik } from 'formik';
import { connect } from 'react-redux';
import * as yup from 'yup';
import {LinearGradient} from "expo-linear-gradient";
import api from '../../Api.js'
import { Style } from "../../source/index";
import practiceExamStyle from "../../source/styles/practiceExamStyle";
import {BlueButton} from "../../source/elements";
import { addExam } from '../../redux/actions/addExamAction';
import { ClearExamState } from '../../redux/actions/addExamAction';
import Dash from "react-native-dash";
import {resetStateStepOne} from "./AddExamStepOne";
import {resetStateStepTwo} from "./AddExamStepTwo";
import {resetStateStepThree} from "./AddExamStepThree";

class AddExamStepFour extends React.Component {
    constructor(props) {
        super(props);
        this.baseState = this.state;
    }

    // Redirect to the previous screen.
    previous = () => {
        this.props.navigation.reset({
            index: 0,
            routes: [{ name: 'BookExamStep03' }],
        })
    }

    resetStates = () => {
        this.setState(this.baseState);
        resetStateStepOne();
        resetStateStepTwo();
        resetStateStepThree();
    };

    // Function to call an alert box.
    ApiCallAlert = () =>
        Alert.alert(
            "OEPS!",
            "Er is iets fout gegaan, probeer het later opnieuw.",
            [
                {
                    text: "Oké",
                    onPress: () => this.props.navigation.navigate("Mijn Examens"),
                }
            ],
            { cancelable: false }
        );

    // Api call to create the exam.
    ApiCall = () => {
        api().post("exam", this.props.bookExam)
        .then(response => {
            if(response.request.status === 200){
                // If the exam is created succesfuly redirect to the "mijn examens" page.
                this.props.addExam(response.data.exam);
                this.props.ClearExamState();
                this.props.navigation.reset({
                    index: 0,
                    routes: [{ name: 'BookExamStep01' }],
                })
                this.resetStates();
                this.props.navigation.navigate("Examens");
            } else {
                // If the request isn't 200 show an alert message.
                this.ApiCallAlert();
            }
        }).catch((error) => {
            // If something else went wrong show an alert message.
            this.ApiCallAlert();
            }
        )
    }

        render() {
        return (
            <View style={{backgroundColor: '#fff' , flex: 1}}>
                <LinearGradient colors={['#F2F2F2', 'transparent']}
                                style={Style.General.gradient}/>
            <ScrollView>
                <View >

                    <Formik
                        // setting formiks intial with the data from the store.
                        initialValues={{
                            Type: this.props.bookExam.exam_type,
                            Datum: this.props.bookExam.date,
                            Locatie: this.props.bookExam.city,
                            Checked: false,
                        }}
                        validationSchema={
                            yup.object().shape({
                                Checked: yup.boolean().oneOf([true], 'U moet de voorwaarden accepteren.'),

                            })
                        }
                        onSubmit={() => {
                            this.ApiCall();
                        }
                    }>
                        {({setFieldValue, handleSubmit, handleChange, errors, values}) => (
                            <View style={Style.General.container}>
                                <Text style={Style.General.h1}>Nieuw examen boeken</Text>
                                <View style={Style.Profile.formField}>
                                <Text style={[Style.General.h6, Style.Profile.formFieldLabel]}>Type Examen:</Text>
                                <TextInput
                                    style={Style.Profile.formFieldInput}
                                    value={values.Type}
                                    editable={false}
                                    selectTextOnFocus={false}
                                />
                                </View>
                                <View style={Style.Profile.formField}>
                                <Text style={[Style.General.h6, Style.Profile.formFieldLabel]}>Examendatum:</Text>
                                <TextInput
                                    style={Style.Profile.formFieldInput}
                                    value={values.Datum}
                                    editable={false}
                                    selectTextOnFocus={false}
                                />
                                </View>
                                <View style={Style.Profile.formField}>
                                <Text style={[Style.General.h6, Style.Profile.formFieldLabel]}>Locatie:</Text>
                                <TextInput
                                    style={Style.Profile.formFieldInput}
                                    value={values.Locatie}
                                    editable={false}
                                    selectTextOnFocus={false}
                                />
                                </View>
                                <View style={Style.Profile.formField}>
                                <Text style={[Style.General.h6, Style.Profile.formFieldLabel]}  >Inschrijfvoorwaarden:</Text>
                                <Text style={Style.ThirdExame.link}
                                      onPress={() => Linking.openURL('https://www.ibki.nl/wp-content/uploads/2018/10/Inschrijfvoorwaarden-IBKI-versie-6-20180828.pdf')}>
                                    http://www.ibki/wp-conent/uploads..
                                </Text>
                                </View>

                                <View>
                                    <Text>Ik heb de voorwaarden gelezen en ga hier mee akkoord</Text>
                                <CheckBox
                                    style={Style.ThirdExame.checkbox}
                                    containerStyle={Style.ThirdExame.checkbox}
                                    title='Akkoord met de voorwaarden.'
                                    checked={values.Checked}
                                    onPress={() => setFieldValue('Checked', !values.Checked)}
                                />
                                <Text style={Style.ThirdExame.errorDisplay}>{errors.Checked}</Text>
                                </View>

                                <View style={Style.ThirdExame.bottom}>
                                    <Text style={[Style.General.h6, Style.Profile.formFieldLabel]}>Betaling:</Text>
                                    <View style={{flexDirection: 'row'}}>
                                        <Text style={[Style.General.title,{flex: 1}]} >Korting door vrijstellingen</Text>
                                        <View style={{alignSelf: "flex-end"}}>
                                            <Text>€0,00</Text>
                                        </View>
                                    </View>
                                    <Dash style={{width: '100%', paddingVertical: 15, height: 1,}} dashGap={0}
                                          dashThickness={1}/>
                                    <View style={{flexDirection: 'row'}}>
                                        <Text style={[Style.General.title,{flex: 1}]}>Te betalen</Text>
                                        <View style={{alignSelf: "flex-end"}}>
                                            <Text>€144,84</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={[Style.ThirdExame.bottom, {marginBottom: 36}]}>
                                <BlueButton
                                    text="Examen Boeken"
                                    onPress={handleSubmit}/>
                                </View>
                            </View>
                        )}
                    </Formik>
                </View>

                <View style={[practiceExamStyle.bottomView, {height: 70,}]}>
                    <TouchableOpacity style={practiceExamStyle.btnPrevious} onPress={this.previous}>
                        <Text>Vorige</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
            </View>
        )
    }
}

// Map reducer state to this.props in class
const mapStateToProps = (state) => {
    return {
        bookExam: state.examsReducer.bookExam,
    }
};

const mapDispatchToProps = {
    addExam,
    ClearExamState
}


export default connect(mapStateToProps, mapDispatchToProps)(AddExamStepFour);
