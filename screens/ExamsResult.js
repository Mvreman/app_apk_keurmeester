import React from 'react';
import {View, Text, FlatList, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {Collapse,CollapseHeader, CollapseBody} from 'accordion-collapse-react-native';
import {Style} from "../source/index";
import Dash from 'react-native-dash';
import moment from 'moment';
import { FontAwesome5 } from '@expo/vector-icons';
import {Ionicons} from '@expo/vector-icons';
import { fetchExams } from '../redux/actions/examsAction'
import {LinearGradient} from "expo-linear-gradient";
import 'moment/locale/nl';



const Exam = ({exam}) => {
    return (
        <View>
            <View style={Style.ShowAllExams.dot}/>
            <View style={Style.ShowAllExams.container_card}>
                <Collapse style={Style.ShowAllExams.card} >
                    <CollapseHeader>
                        <View style={Style.Home.plannedExams} >
                            <View style={Style.ShowAllExams.dateGeplandeExamens}>
                                <Text style={{ fontSize: 20, fontWeight: 'bold'}}>{moment(exam.date).format('DD')}</Text>
                                <Text>{moment(exam.date).format('MMM')}</Text>
                            </View>
                            <View style={Style.ShowAllExams.descriptionGeplandeExamens}>
                                <Text style={Style.ShowAllExams.type}>APK Bevoegedheidsverlenging {exam.branch}</Text>
                                <Text style={{ fontSize: 13}}>Locatie: {exam.city}</Text>
                            </View>
                            <Ionicons name="ios-arrow-down" size={24} color="black" style={Style.ShowAllExams.arrow} />
                        </View>
                    </CollapseHeader>
                    <CollapseBody>
                        <Dash dashGap={0} style={Style.ShowAllExams.dashHorizontal}/>
                        <View style={Style.ShowAllExams.spacingExamsDetail}>
                            <Text>Afname Tijdstip: {exam.date} | {(exam.time).slice(0,-3)}</Text>
                            <Text>Examen nummer: {exam.id}</Text>
                            <Text>Uitslag: {exam.result.result}</Text>
                        </View>
                    </CollapseBody>
                </Collapse>
            </View>
        </View>

    )
};

class ExamsResult extends React.Component {
    componentDidMount() {
        this.props.fetchExams();
    }
    render() {
        return (
            <View style={{flex: 1}}>
                <LinearGradient colors={['#F2F2F2', 'transparent']}
                                style={Style.General.gradient}/>
                <View style={Style.ShowAllExams.viewSpacing}>
                    <Text style={[Style.General.h1, Style.ShowAllExams.flex]}>Mijn Resultaten</Text>
                    <TouchableOpacity style={Style.ShowAllExams.btnAdd}>
                        <FontAwesome5 name="plus" size={18} color="white"/>
                    </TouchableOpacity>
                </View>
                <View style={Style.ShowAllExams.dashContainer}>
                    <Dash dashGap={0}  style={Style.ShowAllExams.dashVertical}/>
                </View>
                <FlatList
                    style={Style.ShowAllExams.flatList}
                    scrollEnabled={true}
                    keyExtractor={(item, index) => index.toString()}
                    data={this.sortDates()}
                    renderItem={({item}) => <Exam exam={item}/>}
                />
                <View />
                <View style={ Style.General.footer }/>
            </View>
        )
    }

    sortDates(){
        let output = [];

        this.props.exams.forEach(check);
        function check(item, index)
        {
            if(item.state === "done")
            {
                output.push(item);
            }
        }
        return output
    }
}


const mapStateToProps = (state) => {

    return {
        exams: state.examsReducer.exams
    }
};

const mapDispatchToProps = {
    fetchExams: fetchExams,
};

export default connect(mapStateToProps, mapDispatchToProps)(ExamsResult);
