import React from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Collapse, CollapseHeader, CollapseBody } from 'accordion-collapse-react-native';
import { Style } from "../source/index";
import Dash from 'react-native-dash';
import moment from 'moment';
import Modal from 'react-native-modal';
import { FontAwesome5 } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import { fetchExams } from '../redux/actions/examsAction'
import { cancelExam } from '../redux/actions/cancelExamAction'
import { LinearGradient } from "expo-linear-gradient";
import 'moment/locale/nl';
import { fetchExamParams } from '../redux/actions/types';
import {BlueButton, RegulationsBTN} from '../source/elements/index';

const Exam = ({ exam, navigation, goToEditExam, setModalVisible }) => {
    return (
        <View>
            <View style={Style.ShowAllExams.dot} />
            <View style={Style.ShowAllExams.container_card}>
                <Collapse style={Style.ShowAllExams.card} >
                    <CollapseHeader>
                        <View style={Style.ShowAllExams.plannedExams} >
                            <View style={Style.ShowAllExams.dateGeplandeExamens}>
                                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{moment(exam.date).format('DD')}</Text>
                                <Text>{moment(exam.date).format('MMM')}</Text>
                            </View>
                            <View style={Style.ShowAllExams.descriptionGeplandeExamens}>
                                <Text style={Style.ShowAllExams.type}>APK Bevoegedheidsverlenging {exam.branch}</Text>
                                <Text style={{ fontSize: 13 }}>Locatie: {exam.city}</Text>
                            </View>
                            <Ionicons name="ios-arrow-down" size={24} color="black" style={Style.ShowAllExams.arrow} />
                        </View>
                    </CollapseHeader>
                    <CollapseBody>
                        <Dash dashGap={0} style={Style.ShowAllExams.dashHorizontal} />
                        <View style={Style.ShowAllExams.spacingExamsDetail}>
                            <Text>Afname Tijdstip: {exam.date} | {(exam.time).slice(0, -3)}</Text>
                            <Text>Examen nummer: {exam.id}</Text>
                            <Text>Geboekt op: {moment(exam.created_at).format('D-M-Y')}</Text>


                            {exam.state === "cancelled" &&
                                <Text style={[Style.General.errorText, { textAlign: 'left' }]}>
                                    Dit examen is geannuleerd
                                </Text>
                            }

                            {exam.state === "planned" &&
                                    <View style={Style.General.column}>
                                        <View style={Style.General.row}>
                                            <BlueButton
                                                style={Style.ShowAllExams.blueBtn}
                                                text="Zet in agenda"
                                                onPress={() => {
                                                    this.props.goToLawSideNav("Diversen");
                                                    this.props.navigation.navigate('Regelgeving', {
                                                        screen: 'RegelgevingSide'
                                                    });
                                                }}/>
                                            <BlueButton
                                                text="Wijzigen"
                                                onPress={() => {
                                                    goToEditExam(exam.id);
                                                    navigation.navigate("EditExam")
                                                }}/>
                                            <BlueButton
                                                text="Anuleren"
                                                onPress={() => setModalVisible(exam.id)}
                                            />
                                        </View>
                                    </View>
                            }

                            {exam.state === "done" &&
                                <View style={{flexDirection: "row", marginTop: 20}}>
                                    <BlueButton
                                        text="Zie resultatem"
                                        onPress={() => {
                                            navigation.navigate("ExamsResult")
                                        }}/>
                                </View>
                            }
                        </View>
                    </CollapseBody>
                </Collapse>
            </View>
        </View>
    )
};

class ShowAllExams extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            cancelExam: null,
            modalVisible: false,
        }
    }

    componentDidMount() {
        this.props.fetchExams();
    }

    /**
     * Hide modal and submit action to the store that is going to cancel the exam
     */
    cancelExam = () => {
        this.props.cancelExam(this.state.cancelExamId);

        this.setModalHidden();
    }

    /**
     * Sets the modal to the visible state
     *
     * @param {number} id
     */
    setModalVisible = (id) => {
        this.setState({
            cancelExamId: id,
            modalVisible: true,
        });
    }

    /**
     * Sets modal to hidden state
     */
    setModalHidden = () => {
        this.setState({
            modalVisible: false
        });
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <LinearGradient colors={['#F2F2F2', 'transparent']}
                    style={Style.General.gradient} />
                <View style={Style.ShowAllExams.viewSpacing}>
                    {this.props.failed === true ?
                        <Text style={{ backgroundColor: '#ff4d4d', color: '#ffffff', height: 20, textAlign: "center" }}>Error:
                        {this.props.error.message}</Text> : null}
                    <Text style={[Style.General.h1, Style.ShowAllExams.flex]}>Mijn Examens</Text>
                    <TouchableOpacity style={Style.ShowAllExams.btnAdd} onPress={() => { this.props.navigation.navigate("Examen toevoegen") }}>
                        <FontAwesome5 name="plus" size={18} color="white" />
                    </TouchableOpacity>
                </View>
                <View style={Style.ShowAllExams.dashContainer}>
                    <Dash dashGap={0} style={Style.ShowAllExams.dashVertical} />
                </View>
                <FlatList
                    style={Style.ShowAllExams.flatList}
                    scrollEnabled={true}
                    keyExtractor={(item, index) => index.toString()}
                    data={this.sortDates()}
                    renderItem={({ item }) => <Exam exam={item} setModalVisible={this.setModalVisible} navigation={this.props.navigation} goToEditExam={this.props.fetchExamParams} />}
                />
                <View />
                <View style={Style.General.footer} />

                <Modal
                    isVisible={this.state.modalVisible}>
                    <View style={Style.Modal.Modal}>
                        <Text style={Style.Modal.Text}>
                            Weet u zeker dat u dit examen wilt annuleren?
                        </Text>
                        <View style={Style.Modal.Buttonholder}>
                            <TouchableOpacity
                                style={[Style.Button.greyStyle, Style.Modal.Button]}
                                onPress={() => this.setModalHidden()}>
                                <Text>Annuleren</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={Style.Button.yellowStyle}
                                onPress={this.cancelExam}>
                                <Text>Jahoor</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }

    sortDates() {
        let pastDates = [];
        let futureDates = [];

        this.props.exams.forEach(PastFuture)

        function PastFuture(item, index) {
            if (item.date < new moment().format('YYYY-MM-DD'))
                pastDates.push(item);
            else {
                futureDates.push(item);
            }
        }

        pastDates = pastDates.sort((a, b) => new moment(a.date).format('YYYYMMDD') - new moment(b.date).format('YYYYMMDD'))
        futureDates = futureDates.sort((a, b) => new moment(a.date).format('YYYYMMDD') - new moment(b.date).format('YYYYMMDD'))
        let dates = futureDates.concat(pastDates)
        return dates
    }
}

const mapStateToProps = (state) => {
    return {
        error: state.examsReducer.error,
        failed: state.examsReducer.failed,
        exams: state.examsReducer.exams
    }
};

const mapDispatchToProps = {
    fetchExams: fetchExams,
    cancelExam: cancelExam,
    fetchExamParams: fetchExamParams
};

export default connect(mapStateToProps, mapDispatchToProps)(ShowAllExams);
