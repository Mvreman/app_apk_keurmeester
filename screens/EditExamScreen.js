import React from 'react';
import {Button, ActivityIndicator, TextInput, View, Text, ScrollView} from 'react-native';
import { connect } from 'react-redux';
import EXAMTYPES from '../constants/EXAMTYPES';
import LOCATIONS from '../constants/LOCATIONS';
import BRANCH from '../constants/BRANCH';
import getExamAction   from '../redux/actions/examAction'
import updateExamAction from '../redux/actions/updateExamAction'
import {fetchClearExam} from '../redux/actions/types';
import DropDownPicker from 'react-native-dropdown-picker';
import { bindActionCreators } from 'redux';
import {Formik } from 'formik';
import * as yup from 'yup'
import {LinearGradient} from "expo-linear-gradient";
import {Style} from "../source";
import {BlueButton} from "../source/elements";


class EditExam extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        branche: null,
        branches: BRANCH.DROPDOWN,
        examType: null,
        examTypes: EXAMTYPES.DROPDOWN,
    }

    this.shouldComponentRender = this.shouldComponentNotRender.bind(this);
  }

  async componentDidMount(){
    this.reRenderSomething= this.props.navigation.addListener('focus', async () => {
        await this.props.fetchExam(this.props.examId);
      });
  }

  componentWillUnmount() {
    this.props.clearExam()
    this.reRenderSomething;
  }

  shouldComponentNotRender() {
    // Show loader when api call is not finished yet
    if(this.props.pending === false) return false;
    return true;
  }

  changeExamBranch = (item) => {
    let examType = null;
    let examTypes = [];

    switch (item.value) {
        case BRANCH.PERSONENAUTOS:
            examTypes = EXAMTYPES.lightweight
            break;
        case BRANCH.BEDRIJFAUTOS_LICHT:
            examTypes = EXAMTYPES.lightweight
            break;
        case BRANCH.BEDRIJFSAUTOS_ZWAAR:
            examTypes = EXAMTYPES.heavyweight
            break;
        case BRANCH.DRIEWIELIGE_MOTORRIJTUIGEN:
            examTypes = EXAMTYPES.lightweight
            break;
        case BRANCH.AANHANGWAGENS:
            examTypes = EXAMTYPES.heavyweight
            break;
        case BRANCH.OVERIGE_REGELGEVING:
            examTypes = EXAMTYPES.lightweight
            break;
        default:
            examTypes = []
    }

    this.setState({
        examTypes,
        examType,
        branche: true
    })
  }

  changeExamType = (item) => {
    this.setState({
        examType: item.value
    })
  }

  previous = () => {
      this.props.navigation.goBack()
  }

  render() {
      if(this.shouldComponentNotRender()) return <View><ActivityIndicator size="large" color="#133B96" /></View>
      return (
          <View style={{backgroundColor: '#fff' , flex: 1}}>
              <LinearGradient colors={['#F2F2F2', 'transparent']}
                              style={Style.General.gradient}/>

                {this.props.updated === true ?
                    <Text style={{backgroundColor: '#2eb82e', color: '#ffffff', height: 20, textAlign: "center"}}>Successfully
                        updated exam!</Text> : null}
                {this.props.failed === true ?
                    <Text style={{backgroundColor: '#ff4d4d', color: '#ffffff', height: 20, textAlign: "center"}}>Error:
                        {this.props.error.message}</Text> : null}
                  <Formik
                      initialValues={{
                          city: this.props.exam.city,
                          exam_type: this.props.exam.exam_type,
                          branch: this.props.exam.branch,
                          date: this.props.exam.date,
                          time: this.props.exam.time,
                          state: "planned",
                      }}
                      validationSchema={
                          yup.object().shape({
                              city: yup
                                  .string()
                                  .required('city is required!'),
                              exam_type: yup
                                  .string()
                                  .required('Exam type is required!'),
                              branch: yup
                                  .string()
                                  .required('Branch is required!'),
                              date: yup
                                  .string()
                                  .required('Date is required!'),
                              time: yup
                                  .string()
                                  .required('Time is required!'),
                              state: yup
                                  .string()
                                  .required('State is required!')
                          })}
                      onSubmit={values => this.props.updateExam(this.props.examId, values)}>
                      {({handleChange, setFieldValue, handleBlur, touched, errors, handleSubmit, values}) => (

                          <View style={Style.General.container}>
                              <Text style={Style.General.h1}>Examen Wijzigen</Text>
                              <Text style={[Style.General.h6, Style.Profile.formFieldLabel]}> Locatie: </Text>
                              <DropDownPicker
                                  items={LOCATIONS.DROPDOWN.map((item, key) => {
                                      return ({label: item.label, value: item.value, key: key})
                                  })}
                                  value={this.props.exam.city}
                                  defaultValue={this.props.exam.city}
                                  onChangeItem={(item) => {
                                      setFieldValue('city', item.value)
                                  }}
                                  containerStyle={{width: "100%", height: 40, marginBottom: 10}}
                                  zIndex={9999}
                              />
                              {touched.city && errors.city &&
                              <Text style={Style.General.errorText}>{errors.city}</Text>
                              }
                              <Text style={[Style.General.h6, Style.Profile.formFieldLabel]}> Branch: </Text>
                              <DropDownPicker
                                  items={this.state.branches}
                                  value={this.props.exam.branch}
                                  defaultValue={this.props.exam.branch}
                                  onChangeItem={item => {
                                      this.changeExamBranch(item);
                                      setFieldValue('branch', item.value);
                                  }}
                                  containerStyle={{width: "100%", height: 40, marginBottom: 10}}
                                  zIndex={9998}
                              />
                              {touched.branch && errors.branch &&
                              <Text style={Style.General.errorText}>{errors.branch}</Text>
                              }
                              <Text style={[Style.General.h6, Style.Profile.formFieldLabel]}> Type examen: </Text>
                              <DropDownPicker
                                  items={this.state.examTypes}
                                  value={this.props.exam.exam_type}
                                  defaultValue={this.props.exam.exam_type}
                                  onChangeItem={item => {
                                    this.changeExamType(item);
                                    setFieldValue('exam_type', item.value);
                                }}
                                  containerStyle={{width: "100%", height: 40, marginBottom: 10}}
                                  zIndex={9997}
                                  />
                              {touched.exam_type && errors.exam_type &&
                              <Text style={Style.General.errorText}>{errors.exam_type}</Text>
                              }
                              <Text style={[Style.General.h6, Style.Profile.formFieldLabel]}> Datum:</Text>
                              <TextInput
                                  underlineColorAndroid="transparent"
                                  placeholder="Datum van examen"
                                  defaultValue={this.props.exam.date}
                                  style={Style.Exames.dateInput}
                                  onChangeText={handleChange('date')}
                              />
                              {touched.date && errors.date &&
                              <Text style={Style.General.errorText}>{errors.date}</Text>
                              }
                              <Text style={[Style.General.h6, Style.Profile.formFieldLabel]}> Time: </Text>
                              <TextInput
                                  underlineColorAndroid="#ffffff"
                                  placeholder="Tijd van examen"
                                  defaultValue={this.props.exam.time}
                                  style={Style.Exames.dateInput}
                                  onChangeText={handleChange('time')}
                              />
                              {touched.time && errors.time &&
                              <Text style={Style.General.errorText}>{errors.time}</Text>
                              }
                              <BlueButton
                              text="Gegevens wijzigen"
                              onPress={handleSubmit}
                              />
                              <BlueButton
                                  text="Anuleren"
                                  onPress={this.previous}
                              />
                          </View>
                      )}
                  </Formik>
              <View style={Style.General.footer}/>
              </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    failed: state.EditExamReducer.failed,
    error: state.EditExamReducer.error,
    exam: state.EditExamReducer.exam,
    pending: state.EditExamReducer.pending,
    updated: state.EditExamReducer.updated,
    examId: state.EditExamReducer.examId
  }
}

const mapDispatchToProps  = dispatch => bindActionCreators({
  fetchExam: getExamAction,
  updateExam: updateExamAction,
  clearExam: fetchClearExam
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(EditExam);
