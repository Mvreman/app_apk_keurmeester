import React from "react";
import { View, Text, Image } from 'react-native';
import {
    DrawerContentScrollView,
    DrawerItemList,
    DrawerItem
} from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/MaterialIcons';


export function DrawerContent(props) {
    return (
        <View style={{flex: 1}}>
            <View>
                <Image
                    style={{
                        width: "90%",
                        resizeMode: "contain",
                        marginHorizontal: "5%",
                        height: 100
                    }}
                    source={require('../assets/logo_print.gif')}
                />
            </View>
            <DrawerContentScrollView { ...props } style={{
                backgroundColor: '#404040',
                height: "100%"
            }}>
                <DrawerItemList {...props}
                    itemStyle={{
                        marginLeft: 0,
                        borderRadius: 0,
                        width: "100%",
                        marginVertical: 0,
                        height: 55,
                        justifyContent: "center",
                    }}
                    labelStyle={{
                        fontFamily: 'Roboto-slab-bold',
                    }}
                />
            </DrawerContentScrollView>
        </View>
    )
}
