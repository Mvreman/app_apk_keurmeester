import React from 'react';
import {View, Text, TouchableOpacity, ScrollView} from 'react-native';
import createPracticeExam from "../../redux/actions/practiceExam/createAction";
import {bindActionCreators} from "redux";
import connect from "react-redux/lib/connect/connect";
import practiceExamStyle from "../../source/styles/practiceExamStyle";
import {LinearGradient} from "expo-linear-gradient";
import {Style} from "../../source";

class CreatePracticeExam extends React.Component {
    render() {
        return (
            <View style={{backgroundColor: '#fff', flex: 1}}>
                <LinearGradient colors={['#F2F2F2', 'transparent']}
                                style={Style.General.gradient}
                />
                <View style={Style.General.container}>
                    <Text style={Style.General.h1}>Oefenvragen examen</Text>
                    <Text style={practiceExamStyle.spacing}>Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                        laboris
                        nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                        velit
                        esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                        sunt in
                        culpa qui officia deserunt mollit anim id est laborum.</Text>
                    <View>
                        <Text>APK bevoegdheidsverlenging lichte voertuigen</Text>
                        <TouchableOpacity style={practiceExamStyle.btnExamType} onPress={() => {
                            this.props.createPracticeExam('al');
                            this.props.navigation.navigate('CasePracticeExam');
                        }}>
                            <Text style={practiceExamStyle.textWhite}>Oefenvragen maken</Text>
                        </TouchableOpacity>

                        <Text>APK bevoegdheidsverlenging zware voertuigen</Text>
                        <TouchableOpacity style={practiceExamStyle.btnExamType} onPress={() => {
                            this.props.createPracticeExam('az');
                            this.props.navigation.navigate('CasePracticeExam');
                        }}>
                            <Text style={practiceExamStyle.textWhite}>Oefenvragen maken</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={Style.General.footer}/>
            </View>
        )
    }
}

/**
 * Map dispatch to props
 *
 * @param dispatch
 * @returns {{createPracticeExam: createPracticeExam}}
 */
const mapDispatchToProps = dispatch => bindActionCreators({
    createPracticeExam: createPracticeExam,
}, dispatch);

export default connect(undefined, mapDispatchToProps)(CreatePracticeExam);
