import React from 'react';
import {View, Text, TouchableOpacity, ScrollView} from 'react-native';
import practiceExamStyle from "../../source/styles/practiceExamStyle";
import {LinearGradient} from "expo-linear-gradient";
import {Style} from "../../source";

class FinishPracticeExam extends React.Component {
    /**
     * Constructor
     *
     * @param props
     */
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={{backgroundColor: '#fff', flex: 1}}>
                <LinearGradient colors={['#F2F2F2', 'transparent']}
                                style={Style.General.gradient}
                />
                <View style={Style.General.container}>

                    <Text style={Style.General.h1}>Afsluiten</Text>
                    <Text>Klik op Ga terug naar het examen om verder te gaan met het beantwoorden van vragen of klik op
                        Bevestig afsluiten om het examen definitief af te sluiten, u kunt dan niet terug naar de vragen.
                    </Text>
                    <View style={practiceExamStyle.marginTop}>
                        <TouchableOpacity style={[practiceExamStyle.bgBlue, practiceExamStyle.btnBackToExam]}
                                          onPress={() => {
                                              this.props.navigation.jumpTo('PracticeQuestions')
                                          }}>
                            <Text style={practiceExamStyle.textWhite}>Ga terug naar het examen</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[practiceExamStyle.bgRed, practiceExamStyle.btnFinishExam]}
                                          onPress={() => {
                                              this.props.navigation.navigate('PracticeExamResult')
                                          }}>
                            <Text style={practiceExamStyle.textWhite}>Bevestig Afsluiten</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={Style.General.footer}/>
            </View>
        )
    }
}

export default FinishPracticeExam;
