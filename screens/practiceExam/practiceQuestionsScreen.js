import React from 'react';
import {View, Text, TouchableOpacity, ScrollView} from 'react-native';
import {MaterialIcons} from '@expo/vector-icons';
import connect from "react-redux/lib/connect/connect";
import CheckBox from 'react-native-check-box'
import {bindActionCreators} from "redux";
import updatePracticeQuestionAction from "../../redux/actions/practiceExam/updateQuestionAction";
import practiceExamStyle from "../../source/styles/practiceExamStyle";
import {LinearGradient} from "expo-linear-gradient";
import {Style} from "../../source";

let i = 0;

function updateStoredPracticeExam(practiceExam, i, answer) {
    practiceExam.practice_questions[i].answer_id = answer.id;
    practiceExam.practice_questions[i].question_result = answer.correct === 1 ? 1 : 0;
}

class PracticeQuestions extends React.Component {
    /**
     * Constructor
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.shouldComponentNotRender = this.shouldComponentNotRender.bind(this);
        this.state = {
            selectedCheckbox: {},
        }
    }

    /**
     * Async function for the update api call of a practice question
     *
     * @returns {Promise<void>}
     */
    async componentDidMount() {
        await this.props.updateAnswer;
    }

    /**
     * Boolean to check if the component should render
     *
     * @returns {boolean}
     */
    shouldComponentNotRender() {
        return this.props.pending !== false;
    }

    /**
     * Sets a selected checkbox to the state
     *
     * @param selectedCheckbox
     * @constructor
     */
    CheckMe = selectedCheckbox => {
        this.setState({selectedCheckbox});
    };

    /**
     * Dynamic render function for practice questions
     *
     * @returns {*}
     */
    renderPracticeQuestion() {
        const {selectedCheckbox} = this.state;
        const answers = this.props.practiceExam.practice_questions[i].answers;
        const practiceExam = this.props.practiceExam;

        return (
            <ScrollView style={{backgroundColor: '#fff',}}>
                <LinearGradient colors={['#F2F2F2', 'transparent']}
                                style={Style.General.gradient}
                />
                <View style={[Style.General.container]}>
                    {this.props.failed === true ?
                        <Text style={{backgroundColor: '#ff4d4d', color: '#ffffff', height: 20, textAlign: "center"}}>Error:
                            {this.props.error.message}</Text> : null}
                    <View style={[Style.General.card, practiceExamStyle.spacing]}>
                        <Text style={Style.General.h1}>Vraag {i + 1}</Text>
                        <Text>{practiceExam.practice_questions[i].question}</Text>
                    </View>
                    <View style={[Style.General.card, practiceExamStyle.fixCard]}>
                        <Text style={[practiceExamStyle.bgYellow, practiceExamStyle.fontWeightBold]}>Antwoord</Text>
                        <View style={Style.Regelgeving.insideCard}>
                            {answers.map((answer, j) => (
                                <CheckBox
                                    key={j}
                                    style={{flex: 1, paddingVertical: 10}}
                                    onClick={() => {
                                        this.CheckMe(answer);
                                    }}
                                    isChecked={answer.id === selectedCheckbox.id}
                                    rightText={answer.answer}
                                />
                            ))}
                        </View>
                    </View>
                    <TouchableOpacity style={practiceExamStyle.btnSideMenu}>
                        <MaterialIcons name="more-vert" size={24} color="white"/>
                    </TouchableOpacity>
                </View>


                <View style={[Style.PracticeExam.bottomView, Style.General.footer]}>
                    {(() => {
                        if (i === 0) {
                            return (
                                <TouchableOpacity style={Style.PracticeExam.btnPrevious} onPress={() => {
                                    this.props.navigation.navigate('CasePracticeExam')
                                }}>
                                    <Text>Vorige</Text>
                                </TouchableOpacity>
                            )
                        } else {
                            return (
                                <TouchableOpacity style={Style.PracticeExam.btnPrevious} onPress={() => {
                                    i -= 1;
                                    this.state.answers = practiceExam.practice_questions[i].answers;
                                    this.forceUpdate()
                                }}>
                                    <Text>Vorige</Text>
                                </TouchableOpacity>
                            )
                        }
                    })()}
                    {(() => {
                        if (i === (this.props.practiceExam.practice_questions.length - 1)) {
                            return (<TouchableOpacity style={Style.PracticeExam.btnNext} onPress={() => {
                                this.props.updateAnswer(practiceExam.id, practiceExam.practice_questions[i].id, selectedCheckbox);
                                updateStoredPracticeExam(practiceExam, i, selectedCheckbox);
                                i = 0;
                                this.props.navigation.navigate('FinishPracticeExam')
                            }}>
                                <Text>Volgende</Text>
                            </TouchableOpacity>)
                        } else {
                            return (<TouchableOpacity style={Style.PracticeExam.btnNext} onPress={() => {
                                this.props.updateAnswer(practiceExam.id, practiceExam.practice_questions[i].id, selectedCheckbox);
                                updateStoredPracticeExam(practiceExam, i, selectedCheckbox);
                                i += 1;
                                this.state.answers = practiceExam.practice_questions[i].answers;
                                this.forceUpdate();
                            }}>
                                <Text>Volgende</Text>
                            </TouchableOpacity>)
                        }
                    })()}
                </View>
            </ScrollView>
        );
    }

    render() {
        return (
            <View>
                {this.renderPracticeQuestion()}
            </View>
        );
    }
}

/**
 * Map state of the reducer to props
 *
 * @param state
 * @returns {{practiceExam: []}}
 */
const mapStateToProps = state => {
    return {
        practiceExam: state.practiceExamReducer.practiceExam,
        failed: state.practiceExamReducer.failed,
        error: state.practiceExamReducer.error
    }
};

const mapDispatchToProps = dispatch => bindActionCreators({
    updateAnswer: updatePracticeQuestionAction,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PracticeQuestions);
