import React from 'react';
import {Text, View, TouchableOpacity, ScrollView, ActivityIndicator} from 'react-native';
import connect from "react-redux/lib/connect/connect";
import {Style} from "../../source";
import {Collapse, CollapseBody, CollapseHeader} from "accordion-collapse-react-native";
import {Ionicons} from "@expo/vector-icons";
import {LinearGradient} from "expo-linear-gradient";
import moment from "moment";
import generalStyle from "../../source/styles/generalStyle";
import practiceExamStyle from "../../source/styles/practiceExamStyle";
import {bindActionCreators} from "redux";
import {clearPracticeExam} from "../../redux/actions/types.js";
import {BlueButton} from "../../source/elements";

let i = 0;

/**
 * Constant for the dropdown of a practice question
 *
 * @param practiceQuestion
 * @returns {*}
 * @constructor
 */
const PracticeQuestion = ({practiceQuestion}) => {
    /**
     * Finds the answer that was given for a practice question
     */
    function findAnswer(answer) {
        return answer.id === practiceQuestion.answer_id;
    }

    let answerGiven = practiceQuestion.answers.find(findAnswer);

    return (
        <View>
            <Collapse style={[Style.General.card, practiceExamStyle.stretch]}>
                <CollapseHeader>
                    <View style={{flexDirection: 'row',}}>
                        <Text style={[Style.General.h1, practiceExamStyle.titleText]}>Vraag {i += 1}</Text>
                        <Text style={practiceExamStyle.points}>{practiceQuestion.question_result} punt(en)
                            behaald</Text>
                        <Ionicons name="ios-arrow-down" size={24} color="black" style={practiceExamStyle.floatRight}/>
                    </View>
                </CollapseHeader>
                <CollapseBody>
                    <View style={Style.ShowAllExams.spacingExamsDetail}>
                        <Text/>
                        <Text>{practiceQuestion.question}</Text>
                        <Text/>
                        <Text style={generalStyle.title}>Mogelijke antwoorden</Text>
                        {practiceQuestion.answers.map((answer, i) => (
                            <Text key={i}>{answer.answer}</Text>
                        ))}
                        <Text/>
                        <Text style={generalStyle.title}>Gegeven antwoorden</Text>
                        <Text>{answerGiven != null ? answerGiven.answer : "Geen antwoord bekend"}</Text>
                        <Text/>
                        <Text style={generalStyle.title}>Feedback</Text>
                        <Text>{practiceQuestion.feedback}</Text>
                    </View>
                </CollapseBody>
            </Collapse>
        </View>
    )
};

class ShowPracticeExam extends React.Component {
    /**
     * Constructor
     *
     * @param props
     */
    constructor(props) {
        super(props);
    }

    /**
     * Clear the redux store by unmounting the component
     */
    componentWillUnmount() {
        this.props.clearPracticeExam
    }

    render() {
        const practiceExam = this.props.practiceExam;

        return (
            <View style={{flex: 1}}>
                <LinearGradient colors={['#F2F2F2', 'transparent']} style={Style.General.gradient}/>
                <ScrollView>
                    <View style={Style.General.container}>
                        <Text style={Style.General.h1}>APK Oefentoetsen LV en ZV</Text>
                        <Text>Examendatum: {moment(practiceExam.created_at).format('D-M-Y')}</Text>
                        <Text/>
                        <View>
                            {practiceExam.practice_questions.map((question) => (
                                <PracticeQuestion key={question.id} practiceQuestion={question}/>
                            ))}

                        </View>
                        <BlueButton
                            text="Terug naar Home"
                            onPress={() => {
                                this.props.clearPracticeExam;
                                this.props.navigation.reset({
                                    index: 0,
                                    routes: [{name: "Home"}]
                                });
                            }}
                        />
                    </View>
                </ScrollView>
                <View style={Style.General.footer}/>
            </View>
        )
    }
}

/**
 * Map state of reducer to props
 *
 * @param state
 * @returns {{practiceExam: []}}
 */
const mapStateToProps = (state) => {
    return {
        practiceExam: state.practiceExamReducer.practiceExam,
    }
};

const mapDispatchToProps = dispatch => bindActionCreators({
    clearPracticeExam: clearPracticeExam,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps())(ShowPracticeExam);
