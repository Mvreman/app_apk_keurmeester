import React from 'react';
import {View, Text, TouchableOpacity, ScrollView} from 'react-native';
import {MaterialIcons} from '@expo/vector-icons';
import connect from "react-redux/lib/connect/connect";
import PracticeQuestions from "./practiceQuestionsScreen";
import practiceExamStyle from "../../source/styles/practiceExamStyle";
import {Style} from "../../source";
import {LinearGradient} from "expo-linear-gradient";

class CasePracticeExam extends React.Component {
    /**
     * Constructor
     *
     * @param props
     */
    constructor(props) {
        super(props);
    }

    render() {
        const practiceExam = this.props.practiceExam;

        return (
            <View style={{backgroundColor: '#fff', flex: 1}}>
                <LinearGradient colors={['#F2F2F2', 'transparent']}
                                style={Style.General.gradient}
                />
                <View style={Style.General.container}>
                    <Text style={Style.General.h1}>Casus</Text>
                    <Text>
                        <Text style={{marginBottom: 20}}>
                            Hierna vindt u 5 oefenvragen voor de
                            toets: {practiceExam.exam_type === 'al' ? "APK bevoegdheidsverlenging lichte voertuigen" : "APK bevoegdheidsverlenging zware voertuigen"}.
                        </Text>
                        <Text style={{marginHorizontal: 20}}>
                            U krijgt de vragen net zo te zien als op het examen, in de opmaak van het toetsprogramma.
                            Als u
                            op Afsluiten geklikt hebt, kunt u uw score zien (Afsluiten is atijd te vinden in het
                            uitklap menu onderaan de pagina). Hiervoor moet u op Bevestiging afsluiten en daarna
                            op Inzien klikken.
                        </Text>
                    </Text>
                    <TouchableOpacity style={practiceExamStyle.btnSideMenu}>
                        <MaterialIcons name="more-vert" size={24} color="white"/>
                    </TouchableOpacity>
                </View>

                <View style={practiceExamStyle.bottomView}>
                    <TouchableOpacity style={practiceExamStyle.btnPrevious} onPress={() =>
                        this.props.navigation.goBack()}>
                        <Text>Vorige</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={practiceExamStyle.btnNext} onPress={() => {
                        this.props.navigation.navigate('PracticeQuestions')
                    }}>
                        <Text>Volgende</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

/**
 * Map state to props
 *
 * @param state
 * @returns {{practiceExam: []}}
 */
const mapStateToProps = (state) => {
    return {
        practiceExam: state.practiceExamReducer.practiceExam
    }
};

export default connect(mapStateToProps)(CasePracticeExam);
