import React from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import connect from "react-redux/lib/connect/connect";
import practiceExamStyle from "../../source/styles/practiceExamStyle";
import {LinearGradient} from "expo-linear-gradient";
import {Style} from "../../source";
import {bindActionCreators} from "redux";
import {clearPracticeExam} from "../../redux/actions/types";

class PracticeExamResult extends React.Component {
    /**
     * Constructor
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            practiceExam: this.props.practiceExam
        }
    }


    /**
     * Clear the redux store by unmounting the component
     */
    componentWillUnmount() {
        this.props.clearPracticeExam
    }

    /**
     * Calculate the result of the total practice exam by counting the points of all the practice questions
     *
     * @returns {number}
     * @constructor
     */
    CalculateResult() {
        try {
            let result = 0;

            this.state.practiceExam.practice_questions.map(key => {
                key.question_result === 1 ? result += 1 : result += 0
            });

            return result;
        } catch (e) {
            console.log(e)
        }
    };

    render() {
        let result = this.CalculateResult();
        let length = this.state.practiceExam.practice_questions.length;

        return (
            <View style={{backgroundColor: '#fff', flex: 1}}>
                <LinearGradient colors={['#F2F2F2', 'transparent']}
                                style={Style.General.gradient}
                />
                <View style={Style.General.container}>
                    <Text style={Style.General.h1}>Afsluiten</Text>
                    <Text>Beste deelnemer,</Text>
                    <Text/>
                    <Text>Bij dit examen zijn {result} van de {length} vragen goed beantwoord.</Text>
                    <Text/>
                    <Text>Met vriendelijke groet,</Text>
                    <Text>IBKI</Text>
                    <Text/>
                    <Text>Klik op Afsluiten om dit scherm te sluiten</Text>
                    <View style={practiceExamStyle.marginTop}>
                        <TouchableOpacity style={[practiceExamStyle.bgBlue, practiceExamStyle.btnBackToExam]}
                                          onPress={() => {
                                              this.props.navigation.navigate('ShowPracticeExam')
                                          }}>
                            <Text style={practiceExamStyle.textWhite}>Inzien</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[practiceExamStyle.bgRed, practiceExamStyle.btnFinishExam]}
                                          onPress={() => {
                                              this.props.clearPracticeExam;
                                              this.props.navigation.reset({
                                                  index: 0,
                                                  routes: [{name: "Home"}]
                                              });
                                          }}>
                            <Text style={practiceExamStyle.textWhite}>Afsluiten</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={Style.General.footer}/>
            </View>
        )
    }
}

/**
 * Map state of reducer to props
 *
 * @param state
 * @returns {{practiceExam: []}}
 */
const mapStateToProps = (state) => {
    return {
        practiceExam: state.practiceExamReducer.practiceExam,
    }
};

const mapDispatchToProps = dispatch => bindActionCreators({
    clearPracticeExam: clearPracticeExam,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps())(PracticeExamResult);
