import React from 'react';
import { LinearGradient } from 'expo-linear-gradient';
import DropDownPicker from 'react-native-dropdown-picker';
import {Text, ScrollView, View,} from 'react-native';
import { Style } from '../../source';
import { RegulationsBTN, PAuto_Front} from '../../source/elements/index';
import Dash from 'react-native-dash';
import { connect } from 'react-redux';
import {createStore} from "redux";
import {goToLawSideNav} from "../../redux/actions/lawAction";
import lawReducer from "../../redux/reducers/lawReducer";

let articleStore = createStore(lawReducer);

 class Personenauto extends React.Component {

     constructor(props) {
         super(props);

         this.state = {
             viewangle: null,
             parts: []
         };
     }

     changeViewAngle(item) {
         let viewAngle = null;
         let parts = null;
         switch (item.value) {
             case 'item1':
                 parts = [
                     require('../../assets/regelgeving/personenauto.png'),
                     "Voorkant",
                     "testet2",
                     "testet3",
                     "testet3",
                     "testet3",
                 ];
                 break;
             case 'item2':
                 parts = [
                     require('../../assets/regelgeving/icon/car.png'),
                     "Achterkant",
                     "testet2",
                     "testet3",
                     "testet3",
                     "testet3",
                 ];

                 break;
         }

         this.setState({
             viewAngle,
             parts,
         });
     }



     render() {
         return (
             <ScrollView style={{ backgroundColor: '#fff'}}>
                 <LinearGradient colors={['#F2F2F2', 'transparent']}
                                 style={Style.General.gradient}/>
                 <View style={Style.General.container}>
                     <View style={Style.Regelgeving.center}>
                         <Text style={Style.General.h1}>Personenauto</Text>
                     </View>
                     <View style={Style.Regelgeving.dropdown}>
                         <DropDownPicker
                             defaultValue="item1"
                             items={[
                                 {label: 'Voorkant', value: 'item1', source: require('../../assets/regelgeving/personenauto.png')},
                                 {label: 'Achterkant', value: 'item2'},
                                 {label: 'Zijkant', value: 'item3'},
                                 {label: 'Onderkant', value: 'item4'},
                                 {label: 'Binnekant', value: 'item5'},
                             ]}
                             defaultIndex={0}
                             zIndex={9999}
                             containerStyle={{height: 40}}
                             onChangeItem={item => this.changeViewAngle(item)}
                         />
                     </View>
                     <PAuto_Front/>
                     <View style={Style.Regelgeving.box_links}>
                         <RegulationsBTN
                             text="Algemeen"
                             onPress={() => {
                                 this.props.goToLawSideNav("Algemeen");
                                 this.props.navigation.navigate('Regelgeving', {
                                     screen: 'RegelgevingSide'
                                     });
                             }}/>
                         <RegulationsBTN
                             text="Algemene bouwwijze van het voertuig"
                             onPress={() => {
                                 this.props.goToLawSideNav("Algemene bouwwijze van het voertuig");
                                 this.props.navigation.navigate('Regelgeving', {
                                     screen: 'RegelgevingSide'
                                 });}
                             }
                         />
                         <RegulationsBTN
                             text="Afmetingen en massa's"
                             onPress={() => {
                                 this.props.goToLawSideNav("Afmetingen en massa's");
                                 this.props.navigation.navigate('Regelgeving', {
                                     screen: 'RegelgevingSide'
                                 });}
                             }

                         />
                         <RegulationsBTN
                             text="Motor en brandstofsystemen"
                             onPress={() => {
                                 this.props.goToLawSideNav("Motor en brandstofsystemen");
                                 this.props.navigation.navigate('Regelgeving', {
                                     screen: 'RegelgevingSide'
                                 });}
                             }
                         />
                         <RegulationsBTN
                             text="Krachtoverbrenging"
                             onPress={() => {
                                 this.props.goToLawSideNav("Krachtoverbrenging");
                                 this.props.navigation.navigate('Regelgeving', {
                                     screen: 'RegelgevingSide'
                                 });}
                             }
                         />
                         <Dash style={{width:'100%', paddingVertical:10, height:1}} dashGap={0} />
                         <RegulationsBTN
                             text="Assen"
                             onPress={() => {
                                 this.props.goToLawSideNav("Assen");
                                 this.props.navigation.navigate('Regelgeving', {
                                     screen: 'RegelgevingSide'
                                 });
                             }}/>
                         <RegulationsBTN
                             text="Ophanging"
                             onPress={() => {
                                 this.props.goToLawSideNav("Ophanging");
                                 this.props.navigation.navigate('Regelgeving', {
                                     screen: 'RegelgevingSide'
                                 });
                             }}/>
                         <RegulationsBTN
                             text="Stuurinrichting"
                             onPress={() => {
                                 this.props.goToLawSideNav("Stuurinrichting");
                                 this.props.navigation.navigate('Regelgeving', {
                                     screen: 'RegelgevingSide'
                                 });
                             }}/>
                         <RegulationsBTN
                             text="Reminrichting"
                             onPress={() => {
                                 this.props.goToLawSideNav("Reminrichting");
                                 this.props.navigation.navigate('Regelgeving', {
                                     screen: 'RegelgevingSide'
                                 });
                             }}/>
                         <RegulationsBTN
                             text="Carrosserie"
                             onPress={() => {
                                 this.props.goToLawSideNav("Carrosserie");
                                 this.props.navigation.navigate('Regelgeving', {
                                     screen: 'RegelgevingSide'
                                 });
                             }}/>
                         <Dash style={{width:'100%', paddingVertical:10, height:1}} dashGap={0} />
                         <RegulationsBTN
                             text="Lichten, lichtsignalen en retroreflecterende voorzieningen"
                             onPress={() => {
                                 this.props.goToLawSideNav("Lichten, lichtsignalen en retroreflecterende voorzieningen");
                                 this.props.navigation.navigate('Regelgeving', {
                                     screen: 'RegelgevingSide'
                                 });
                             }}/>
                         <RegulationsBTN
                             text="Verbinding personenauto en aanhangwagen"
                             onPress={() => {
                                 this.props.goToLawSideNav("Verbinding personenauto en aanhangwagen");
                                 this.props.navigation.navigate('Regelgeving', {
                                     screen: 'RegelgevingSide'
                                 });
                             }}/>
                         <RegulationsBTN
                             text="Diversen"
                             onPress={() => {
                                 this.props.goToLawSideNav("Diversen");
                                 this.props.navigation.navigate('Regelgeving', {
                                     screen: 'RegelgevingSide'
                                 });
                             }}/>
                         <RegulationsBTN
                             text="Aanvullende eisen taxi's"
                             onPress={() => {
                                 this.props.goToLawSideNav("Aanvullende eisen taxi's");
                                 this.props.navigation.navigate('Regelgeving', {
                                     screen: 'RegelgevingSide'
                                 });
                             }}/>
                     </View>
                 </View>
                 <View style={Style.General.footer}/>
             </ScrollView>
        );
    }
 }

const mapStateToProps = (state) => {

    return {
        state: state.lawReducer.state,
    }
};

const mapDispatchToProps = {
    goToLawSideNav: goToLawSideNav,
};

export default connect(mapStateToProps, mapDispatchToProps)(Personenauto);
