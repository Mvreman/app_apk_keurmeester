import React from 'react';
import {View, Text, FlatList, ScrollView} from 'react-native';
import json from '../../assets/regelgeving/json/personenautos.json'
import {Style} from "../../source";
import {Collapse,CollapseHeader, CollapseBody} from 'accordion-collapse-react-native';
import {connect, useDispatch} from "react-redux";
import Dash from "react-native-dash";
import {GoBackBTN} from "../../source/elements";
import {LinearGradient} from "expo-linear-gradient";

const Law = ({law}) => {
    return (
        <View>
            <Dash style={{width:'100%', paddingVertical:15, height:1,}} dashGap={0} />
            <Text>{law.law_text}</Text>
            <Dash style={{width: '100%', paddingVertical:15, height: 1,}} dashGap={0} dashThickness={1}/>
            <View style={Style.General.cardRow}>
                {law.Wijze_van_keuren !== undefined &&
                <Collapse>
                    <CollapseHeader style={Style.Regelgeving.cardHead}>
                        <Text style={Style.General.title}>Wijze van keuren</Text>
                    </CollapseHeader>
                    <CollapseBody>
                        <Text >{law.Wijze_van_keuren}</Text>
                    </CollapseBody>
                </Collapse>
                }
                {law.Toelichting !== undefined &&
                <Collapse>
                    <CollapseHeader style={Style.Regelgeving.cardHead}>
                        <Text style={Style.General.title}>Toelichting</Text>
                    </CollapseHeader>
                    <CollapseBody >
                        <Text>{law.Toelichting}</Text>
                    </CollapseBody>
                </Collapse>
                }
                {law.Aanvullende_permanente_eisen !== undefined &&
                <Collapse >
                    <CollapseHeader style={Style.Regelgeving.cardHead}>
                        <Text style={Style.General.title}>Aanvulle eisen</Text>
                    </CollapseHeader>
                    <CollapseBody>
                        <Text>{law.Aanvullende_permanente_eisen}</Text>
                    </CollapseBody>
                </Collapse>
                }
            </View>
        </View>

    )
};


class RegelgevingShow extends React.Component
{
    render()
    {
        let lawArticle = json[this.props.article];
        let firstKey  = Object.keys(lawArticle)[0];
        return (
            <View style={{backgroundColor: '#fff', flex: 1}}>
                <LinearGradient colors={['#F2F2F2', 'transparent']} style={Style.General.gradient}/>
                <ScrollView>
                    <View style={Style.General.container}>
                        <Text style={[Style.General.h2, Style.General.title]}>{this.props.article}</Text>
                        <View>
                            <Text style={[Style.General.h6, Style.Regelgeving.bottom]}>{lawArticle[firstKey].law_section_number}</Text>
                            <View style={[Style.General.card, Style.Regelgeving.fixCard]}>
                                <GoBackBTN onPress={() => this.props.navigation.goBack()}/>
                                <View style={Style.Regelgeving.insideCard}>
                                    <Text style={[Style.General.h6, Style.General.title]}>Actuele regelgeving</Text>
                                    {Object.entries(lawArticle).map(([key, val]) => (
                                        <Law law={val}
                                             key={key}
                                        />
                                    ))}
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <View style={Style.General.footer}/>
            </View>
        )
    }
}

 const mapStateToProps = (state) => {

     return {
         article: state.lawReducer.article
     }
 };

export default connect(mapStateToProps)(RegelgevingShow);
