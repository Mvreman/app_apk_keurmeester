import React from 'react';
import {View, Text, FlatList, ScrollView} from 'react-native';
import json from '../../assets/regelgeving/json/sideNav.json'
import {Style} from "../../source";
import {connect} from "react-redux";
import {RegulationsBTN, GoBackBTN } from "../../source/elements";
import {createStore} from "redux";
import lawReducer from "../../redux/reducers/lawReducer";
import {goToLawArticle, goToLawSideNav} from "../../redux/actions/lawAction";
import {LinearGradient} from "expo-linear-gradient";

let articleStore = createStore(lawReducer);

const Law = ({law, nav, type}) => {
    return (
        <View>
            <RegulationsBTN
                text={law.title}
                onPress={() => {
                    type(law.title);
                    nav.navigate('Regelgeving', {
                        screen: 'RegelgevingShow'
                    });
                }}/>
        </View>
    )
};

class RegelgevingSide extends React.Component {
    constructor(props) {
        super(props);
    }
    render()
    {
        let OptionsArticle = json[this.props.sideNav];
        const nav = this.props.navigation;
        const type = this.props.goToLawArticle;
        return (
            <View style={{backgroundColor: '#fff', flex: 1}}>
                <LinearGradient colors={['#F2F2F2', 'transparent']} style={Style.General.gradient}/>
                <ScrollView>
                    <View style={Style.General.container}>
                        <Text style={[Style.General.h2, Style.General.title]}>{this.props.sideNav}</Text>
                        <View style={[Style.General.card, Style.Regelgeving.fixCard]}>
                            <GoBackBTN onPress={() => this.props.navigation.goBack()}/>
                            <View style={Style.Regelgeving.insideCard}>
                                {Object.entries(OptionsArticle).map(([key, val]) => (
                                    <Law law={val}
                                         key={key}
                                         nav={nav}
                                         type={type}
                                    />
                                ))}
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <View style={Style.General.footer}/>
            </View>
        )
    }
}

const mapStateToProps = (state) => {

    return {
        sideNav: state.lawReducer.sideNav,
        state: state.lawReducer.state,
    }
};

const mapDispatchToProps = {
    goToLawArticle: goToLawArticle,
};

export default connect(mapStateToProps, mapDispatchToProps)(RegelgevingSide);
