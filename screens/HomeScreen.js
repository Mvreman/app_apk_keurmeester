import React from 'react';
import {View, Text, Image, ScrollView, ActivityIndicator} from 'react-native';
import { Style } from "../source/index";
import { RDWButton, BlueButton } from '../source/elements/index';
import { LinearGradient } from 'expo-linear-gradient';
import { connect } from "react-redux";
import moment from "moment";
import { bindActionCreators } from "redux";
import { fetchMessages } from "../redux/actions/messageAction";
import { fetchExamsHome } from "../redux/actions/examsHomeAction";
import createPracticeExam from "../redux/actions/practiceExam/createAction";

class Home extends React.Component {
    /**
     * Constructor
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.shouldComponentNotRender = this.shouldComponentNotRender.bind(this);
    }

    /**
     * Async function for the update api call of messages and examsHome
     *
     * @returns {Promise<void>}
     */
    async componentDidMount() {
        await this.props.fetchMessages(this.props.user);
        await this.props.fetchExamsHome();
    }

    /**
     * Boolean to check if the component should render
     *
     * @returns {boolean}
     */
    shouldComponentNotRender() {
        return !(this.props.pending === false && this.props.homeExamsPending === false);
    }

    /**
     * Dynamic render function for homePage
     *
     * @returns {*}
     */
    PracticeZwaar = () => {
        this.props.createPracticeExam('al');
        this.props.navigation.navigate('PracticeExam', { screen: 'CreatePracticeExam'});
    }
    render()
    {
        if (this.shouldComponentNotRender()) return <View><ActivityIndicator size="large" color="#133B96"/></View>
        // Slicing examsHome and messages so only 3 are displayed
        const slicedmessages = this.props.messages.slice(0,3);
        const slicedexams = this.props.exams.slice(0,3);
        return (
            <ScrollView nestedScrollEnabled={true} style={{backgroundColor: '#fff'}}>
                <LinearGradient colors={['#F2F2F2', 'transparent']}
                                style={Style.General.gradient}
                />
                <View style={Style.General.container}>
                    {this.props.messageFailed === true ?
                    <Text style={{backgroundColor: '#ff4d4d', color: '#ffffff', height: 20, textAlign: "center"}}>Message error:
                        {this.props.messageError.message}</Text> : null}
                    {this.props.examsFailed === true ?
                    <Text style={{backgroundColor: '#ff4d4d', color: '#ffffff', height: 20, textAlign: "center"}}>Exams error:
                        {this.props.examsError.message}</Text> : null}
                    <View style={Style.General.viewSpacing}>
                        <Text style={Style.General.h1}>Welkom bij Mijn IBKI</Text>
                        <View style={Style.Home.topBoxes}>
                            <View style={[Style.Home.topBoxLeft, Style.General.card]}>
                                <Text style={Style.General.text}>{this.props.user.name ? this.props.user.name : 'Naam niet gevonden'}</Text>
                                <Text style={Style.General.text}>{this.props.user.pass_number ? this.props.user.pass_number : 'Pasnummer niet gevonden'}</Text>
                                <Text style={Style.General.text}>APK Keurmeester</Text>
                                <Text style={Style.General.text}>Keurbevoegdheid</Text>
                                <Text style={Style.General.text}>{ this.props.user.jurisdictions ? moment(this.props.user.jurisdictions[0].end_date).format('D MMMM YYYY') : 'end date niet gevonden' }</Text>
                            </View>
                            <View style={[Style.Home.topBoxRight, Style.General.card]}>
                                <Text style={Style.General.text} >Cusumstand</Text>
                                <Text style={Style.General.text}>{ this.props.user.jurisdictions ? moment(this.props.user.jurisdictions[0].created_at).format('YYYY') : 'end date niet gevonden' } / { this.props.user.jurisdictions ? moment(this.props.user.jurisdictions[0].end_date).format('YYYY') : 'end date niet gevonden' }</Text>
                                <Text>
                                    <Text style={[Style.General.text, { textTransform: "uppercase" }]}>{ this.props.user.jurisdictions ? this.props.user.jurisdictions[0].jurisdiction_type : 'Type niet gevonden' }: </Text>
                                    <Text style={Style.General.text}>{ this.props.user.jurisdictions ? this.props.user.jurisdictions[0].cusum_points : 'Cusumstand niet gevonden' } | 10</Text>
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View style={Style.General.viewSpacing}>
                        <Image
                            source={require('../assets/personenauto-voor-RDW.png')}
                            style={{width: '100%', resizeMode: 'contain'}}
                        />
                        <View style={{marginHorizontal: '10%', marginTop: 20}}>
                            <RDWButton
                                text="Online APK-regelgeving"
                                onPress={() => this.props.navigation.navigate('Regelgeving')}
                            />
                        </View>
                    </View>
                    <View style={Style.General.viewSpacing}>
                        <Text style={Style.General.h2}>Geplande examens</Text>
                        {/*Showing 3 exams and binding data to the Text components*/}
                        {slicedexams.map((key) => (
                            <View key={key.id} style={[Style.General.card, Style.Home.plannedExams]}>
                                <View style={Style.Home.datePlannedExams}>
                                    <Text style={{fontSize: 20, fontWeight: 'bold'}}>{moment(key.date).format('DD')}</Text>
                                    <Text>{moment(key.date).format('MMM')}</Text>
                                </View>
                                <View style={Style.Home.descriptionPlannedExams}>
                                    <Text >APK Bevoegedheidsverlenging {key.branch}</Text>
                                    <Text style={{ fontSize: 13}}>Locatie: {key.city}</Text>
                                </View>
                            </View>
                        ))}
                        <Text style={Style.Home.show} onPress={() => this.props.navigation.navigate('Mijn Examens')}>Bekijk geplande examens ></Text>
                    </View>
                    <View style={Style.General.viewSpacing}>
                        <Image
                            source={require('../assets/tumbnail-video.png')}
                            style={{ width: '100%', height: 225, resizeMode: 'contain' }}
                        />
                    </View>
                    <View>
                        <View style={Style.General.viewSpacing}>
                            <Text style={Style.General.h2}>Berichten box</Text>
                            {/*Showing 3 messages and binding data to the Text components*/}
                            {slicedmessages.map((index) => (
                                <View key={index.id} style={Style.Home.messageBox}>
                                    <View style={Style.Home.messageInfo}>
                                        <View style={Style.Home.message}>
                                            <View style={Style.Home.messageInfo}>
                                                <Text style={{fontWeight: 'bold'}}>{index.title}</Text>
                                                <Text numberOfLines={2}>{index.text}</Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            ))}
                            <Text style={Style.Home.show} onPress={() => this.props.navigation.navigate('Berichten')}>Bekijk alle berichten ></Text>
                        </View>
                    </View>
                    <View style={Style.General.viewSpacing}>
                        <View style={Style.Home.homePractice}>
                            <Text style={Style.General.h2}>Klik hier om te oefenen</Text>
                            <Text style={{textAlign: 'center'}}>Wil je zeker weten dat jij jouw examen gaat halen en
                                jouw bevoegdheden kunt verlengen?{"\n"}{"\n"}Oefen dan door onze oefenexamens te maken.
                            </Text>
                            <View style={Style.Home.homePracBtns}>
                                <BlueButton
                                    text="APK Licht"
                                    onPress={() => {
                                        this.props.createPracticeExam('al');
                                        this.props.navigation.navigate('Oefenvragen', { screen: 'CasePracticeExam'});
                                    }}
                                />
                                <BlueButton
                                    text="APK Zwaar"
                                    onPress={() => {
                                        this.props.createPracticeExam('az');
                                        this.props.navigation.navigate('Oefenvragen', { screen: 'CasePracticeExam'});
                                    }}
                                />
                            </View>
                        </View>
                    </View>
                </View>
                <View style={Style.General.footer} />
            </ScrollView>
        );
    }
}

/**
 *
 * @param state
 * @returns {{homeExamsPending: boolean, examsError: *, exams: [], messageFailed: *, pending: *, messages: (string|string|TestMessage[]|undefined|[]), messageError: *, examsFailed: *, user: []}}
 */
const mapStateToProps = (state) => {{
    return {
        user: state.loginReducer.login,
        messages: state.messageReducer.messages,
        pending: state.messageReducer.pending,
        messageError: state.messageReducer.error,
        messageFailed: state.messageReducer.failed,
        examsError: state.examsHomeReducer.error,
        examsFailed: state.examsHomeReducer.failed,
        exams: state.examsHomeReducer.exams,
        homeExamsPending: state.examsHomeReducer.homeExamsPending,
    }
}};

/**
 *
 * @param dispatch
 * @returns {{fetchExamsHome: (function(): function(...[*]=)), fetchMessages: (function(): function(...[*]=)), createPracticeExam: createPracticeExam}}
 */
const mapDispatchToProps = dispatch => bindActionCreators( {
    fetchMessages: fetchMessages,
    fetchExamsHome: fetchExamsHome,
    createPracticeExam: createPracticeExam,
}, dispatch);

export default connect(mapStateToProps,mapDispatchToProps)(Home);
