import React, { useState } from 'react'
import { View, Text, TextInput, Button, ScrollView } from 'react-native'
import { Style } from "../source/index";
import { updateUser } from "../redux/actions/userAction"
import { useSelector, useDispatch } from 'react-redux';
import { LinearGradient } from "expo-linear-gradient";
import { Formik } from 'formik';
import * as yup from 'yup';
import {BlueButton} from "../source/elements";


/**
 * React Native component for the profile page.
 *
 * @param  {} {navigation}
 * @param  {} props
 */
export default function Profile({ navigation }, props) {

    //Used for calling Redux actions.
    const dispatch = useDispatch();

    //Attaches the login part of the Redux store to local variable user.
    const user = useSelector(state => state.loginReducer.login);

    //Bool to determine wether the profile page is in edit mode or not.
    const [EditMode, setEditMode] = useState(false)


    const validationRules = yup.object({
        pass_number: yup.string()
            .required('Uw pasnummer kan niet leeg zijn')
            .matches(/^[0-9]+$/, 'Uw pasnummer mag alleen cijfers bevatten')
            .max(20, 'Uw pasnummer kan niet langer dan 20 cijfers zijn'),
        email: yup.string()
            .required('Uw mailadres kan niet leeg zijn')
            .email(),
        phone_number: yup.string()
            .required('Uw telefoonnummer kan niet leeg zijn')
            .max(9, 'Uw telefoonnummer kan niet langer dan 9 cijfers zijn')
            .matches(/^[0-9]+$/, 'Uw telefoonnummer mag alleen cijfers bevatten'),
        name: yup.string()
            .required('Uw naam kan niet leeg zijn')
            .matches(/[a-z\sA-Z]+$/,"Uw naam mag alleen letters en spaties bevatten."),
        street: yup.string()
            .required('Uw straat kan niet leeg zijn')
            .matches(/[a-z\sA-Z]+$/,"Uw straat mag alleen letters en spaties bevatten."),
        house_number: yup.string()
            .required('Uw huisnummer kan niet leeg zijn'),
        residence: yup.string()
            .required('Uw woonplaats kan niet leeg zijn')
            .matches(/[a-z\sA-Z]+$/,"Uw plaatsnaam mag alleen letters en spaties bevatten."),
        zip_code: yup.string()
            .required('Uw postcode kan niet leeg zijn')
            .matches(/[0-9]{4}[\s]?[a-zA-Z]{2}$/,'Uw postcode is incorrect ingevuld. (Bijv. 2025 GS of 2025GS)')
            .max(7, 'Uw postcode kan niet langer dan 7 karakters zijn')

    })

    /**
     * Function to toggle edit mode when the corresponding button is pressed.
     */
    function ToggleEditMode() {
        if (!EditMode) {
            setEditMode(true)
        }
        else {
            setEditMode(false)
        }
    }

    return (
        <View style={{backgroundColor: '#fff', flex: 1}}>
            <LinearGradient colors={['#F2F2F2', 'transparent']}
                            style={Style.General.gradient}/>
                            <ScrollView>
            <View style={Style.General.container}>
                <Formik
                    validationSchema={validationRules}
                    initialValues={{
                        pass_number: user.pass_number ? user.pass_number : "Loading...",
                        email: user.email ? user.email : "Loading...",
                        phone_number: user.user_details.phone_number ? user.user_details.phone_number.toString() : "Loading...",
                        name: user.name ? user.name : "Loading...",
                        street: user.user_details.street ? user.user_details.street : "Loading...",
                        house_number: user.user_details.house_number ? user.user_details.house_number : "Loading...",
                        residence: user.user_details.residence ? user.user_details.residence : "Loading...",
                        zip_code: user.user_details.zip_code ? user.user_details.zip_code : "Loading..."

                    }}
                    onSubmit={values => {

                        //Add values not being shown in the actual form that need to be sent to the API anyway.
                        values.date_of_birth = user.user_details.date_of_birth
                        values.ssn = user.user_details.ssn

                        //Call Redux action to update the user.
                        dispatch(updateUser(values))

                        //Go back out of edit mode.
                        setEditMode(false)
                    }}>
                    {({ handleChange,resetForm, handleBlur, handleSubmit, values, touched, errors }) => (
                        <View>
                        <Text style={Style.General.h1}>Mijn profiel</Text>
                <View style={Style.Profile.formField}>
                    <Text style={[Style.General.h6, Style.Profile.formFieldLabel]}>
                        IBKI relatienummer
                    </Text>
                    <TextInput
                    editable={false}
                    onChangeText={handleChange('pass_number')}
                    value={values.pass_number}
                    style={Style.Profile.formFieldInput}
                    />
                </View>
                <View style={Style.Profile.formField}>
                    <Text style={[Style.General.h6, Style.Profile.formFieldLabel]}>
                        Volledige naam
                    </Text>
                    <TextInput
                    editable={false}
                    onChangeText={handleChange('name')}
                    value={values.name}
                    style={Style.Profile.formFieldInput}
                    />
                </View>

                <View style={Style.Profile.formField}>
                    <Text style={[Style.General.h6, Style.Profile.formFieldLabel]}>
                        Emailadres
                    </Text>
                    <TextInput
                    editable={EditMode}
                    onChangeText={handleChange('email')}
                    value={values.email}
                    style={Style.Profile.formFieldInput}
                    />
                    {errors.email
                        ?  <Text style={Style.General.errorText}>{errors.email}</Text>
                        : null
                    }
                </View>
                <View style={Style.Profile.formField}>
                    <Text style={[Style.General.h6, Style.Profile.formFieldLabel]}>
                        Telefoonnummer
                    </Text>
                    <TextInput
                    keyboardType={'phone-pad'}
                    editable={EditMode}
                    onChangeText={handleChange('phone_number')}
                    value={values.phone_number}
                    style={Style.Profile.formFieldInput}
                    />
                    {errors.phone_number
                        ? <Text style={Style.General.errorText}>{errors.phone_number }</Text>
                        : null
                    }
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <View style={[Style.Profile.formField, {flex: 0.5}]}>
                    <Text style={[Style.General.h6, Style.Profile.formFieldLabel]}>
                        Straat
                    </Text>
                    <TextInput
                    editable={EditMode}
                    onChangeText={handleChange('street')}
                    value={values.street}
                    style={Style.Profile.formFieldInput}
                    />
                    {errors.street
                        ? <Text style={Style.General.errorText}>{errors.street }</Text>
                        : null
                    }
                </View>
                <View style={[Style.Profile.formField, {flex: 0.28}]}>
                    <Text style={[Style.General.h6, Style.Profile.formFieldLabel ]}>
                        Huisnr.
                    </Text>
                    <TextInput
                    editable={EditMode}
                    onChangeText={handleChange('house_number')}
                    value={values.house_number}
                    style={Style.Profile.formFieldInput}
                    />

                </View>
                </View>
                {errors.house_number
                   ? <Text style={Style.General.errorText}>{errors.house_number}</Text>
                   : null
                }
                <View style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom: 15}}>
                    <View style={[Style.Profile.formField, {flex: 0.5}]}>
                        <Text style={[Style.General.h6, Style.Profile.formFieldLabel]}>
                            Plaats
                        </Text>
                        <TextInput
                        editable={EditMode}
                        onChangeText={handleChange('residence')}
                        value={values.residence}
                        style={Style.Profile.formFieldInput}
                        />
                        {errors.residence
                            ? <Text style={Style.General.errorText}>{errors.residence }</Text>
                            : null
                        }
                    </View>
                    <View style={[Style.Profile.formField, {flex: 0.28}]}>
                        <Text style={[Style.General.h6, Style.Profile.formFieldLabel]}>
                            Postcode
                        </Text>
                        <TextInput
                        editable={EditMode}
                        onChangeText={handleChange('zip_code')}
                        value={values.zip_code}
                        style={Style.Profile.formFieldInput}
                        />
                    </View>
                </View>
                {errors.zip_code
                    ? <Text style={Style.General.errorText}>{errors.zip_code }</Text>
                    : null
                 }
                {/* If edit mode is set to true, display the save button. */}
                {EditMode
                ? <BlueButton text="Opslaan" onPress={handleSubmit}/>
                    : null
                }
                {/* Edit mode toggle button.
                 Changes text from "Mijn profiel wijzigen" to "Annuleren" whenever edit mode is enabled. */}
                <BlueButton
                    text={EditMode? "Annuleren" : "Mijn profiel wijzigen"}
                    onPress={() => {
                        if(EditMode){
                            resetForm()
                        }
                        ToggleEditMode()
                    }}
                />
                </View>)}
                </Formik>
            </View>
            </ScrollView>
            <View style={Style.General.footer}/>
        </View>
    )
};
