import React from 'react';
import { View } from 'react-native';
import { connect } from "react-redux";

import { logout } from "../../redux/actions/logoutAction";

class Logout extends React.Component {
    constructor(props) {
        super(props);
    };

    async componentDidMount() {
        await this.props.logoutAction();
        this.props.navigation.navigate('Login');
    };

    render() {
        return (
            <View style={{backgroundColor: '#222222'}}>

            </View>
        );
    };
}

const mapDispatchToProps = {
    logoutAction: logout,
}
// Map login reducer state to be accessible via this.props
const mapStateToProps = (state) => {
    return {
        login: state.loginReducer.login,
    }
}

// Add props to the view.
export default connect(mapStateToProps, mapDispatchToProps)(Logout);
