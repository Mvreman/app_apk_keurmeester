import React from 'react';
import { View, Text, TextInput, Keyboard, ToastAndroid, Platform, ScrollView, ImageBackground, Image, Dimensions, StyleSheet } from 'react-native';
import { connect } from "react-redux";
import { Style } from "../../source";
import { Formik } from 'formik';
import * as yup from 'yup';

import { postLogins } from "../../redux/actions/loginsAction";
import { YellowButton } from "../../source/elements";

// This schema contains all the validation rules to be applied on submit.
const loginSchema = yup.object({
    pass_number: yup.string()
        .required('Uw pasnummer kan niet leeg zijn')
        .matches(/^[0-9]+$/, 'Uw pasnummer mag alleen cijfers bevatten')
        // .min(6, 'Uw pasnummer moet minimaal 6 cijfers lang zijn')
        .max(20, 'Uw pasnummer kan niet langer dan 20 cijfers zijn'),
    password: yup.string()
        .required('Uw wachtwoord kan niet leeg zijn')
        .min(8, 'Uw wachtwoord moet minimaal 8 karakters hebben')
        .max(20, 'Uw wachtwoord kan niet langer dan 20 karakters zijn')
        .matches(/^.(?=.{7,})(?=.*[a-zA-Z])(?=.*\d).*$/, 'Uw wachtwoord moet uit letters en nummers bestaan')
        .matches(/^[^\s]*$/, 'Uw wachtwoord mag geen whitespace bevatten'),
});

// This page accesses and interacts with Redux using a Class Component.
class Login extends React.Component {

    state = { disable_btn: false };

    render() {
        return (
            <ScrollView scrollEnabled={false} style={Height.screenheight}>
                <ImageBackground source={require('../../assets/login-background.png')} style={[Style.Auth.backgroundImage, Height.screenheight]}>
                    <View style={[Style.Auth.loginContainer, Style.Auth.alignment]}>
                        {this.props.failed === true ?
                        <Text style={{backgroundColor: '#ff4d4d', color: '#ffffff', height: 20, textAlign: "center"}}>Error:
                            {this.props.error.message}</Text> : null}
                        <View style={Style.Auth.inputContainer}>
                            <Image source={require('../../assets/logo_print.gif')} style={Style.Auth.logo} />
                            <Text style={Style.General.h1}>Inloggen bij mijn IBKI</Text>
                            <Formik
                                initialValues={{ pass_number: '', password: '', }}
                                validationSchema={loginSchema}
                                onSubmit={ async (values, { resetForm }) => {
                                    this.setState({ disable_btn: true });
                                    Keyboard.dismiss();
                                    await this.props.postLogins(values);
                                    // This works on the basis that only a successfully logged in user will return a non-empty object.

                                    if (typeof this.props.login === 'object' && this.props.login !== null && Array.isArray(this.props.login) !== true) {
                                        resetForm();
                                        this.props.navigation.navigate(
                                            'Home'
                                        );
                                    } else {
                                        this.setState({ disable_btn: false });
                                        // TODO: Find an IOS equivalent to Toast.
                                        switch (Platform.OS) {
                                            case "ios": {
                                                return;
                                            }
                                            case "android": {
                                                return ToastAndroid.show('Probeer opnieuw in te loggen.', ToastAndroid.LONG);
                                            }
                                            default:
                                                return;
                                        }
                                    }
                                }}
                            >
                            {(props) => (
                                <View>
                                    <TextInput
                                        style={Style.Auth.input}
                                        placeholder='Pasnummer'
                                        onChangeText={props.handleChange('pass_number')}
                                        value={props.values.pass_number}
                                        onBlur={props.handleBlur('pass_number')}
                                        keyboardType='numeric'
                                    />
                                    <Text style={Style.General.errorText}>{ props.touched.pass_number && props.errors.pass_number }</Text>

                                    <TextInput
                                        style={Style.Auth.password}
                                        placeholder='Wachtwoord'
                                        onChangeText={props.handleChange('password')}
                                        value={props.values.password}
                                        onBlur={props.handleBlur('password')}
                                        keyboardType='default'
                                        // Password rules for iOS automatic passwords. TODO: CANT TEST THIS, SOMEONE WITH IOS TRY THIS OUT.
                                        passwordRules='required: lower; required: upper; required: digit; required: [-]; minlength: 8; maxlength: 20;'
                                        secureTextEntry={true}
                                    />
                                    <Text style={Style.General.errorText}>{ props.touched.password && props.errors.password }</Text>

                                    <YellowButton
                                        text="Inloggen"
                                        onPress={props.handleSubmit}
                                        disabled={this.state.disable_btn}
                                    />
                                </View>
                            )}
                            </Formik>
                        </View>
                    </View>
                </ImageBackground>
            </ScrollView>
        );
    };
}

const mapDispatchToProps = {
    postLogins,
}
// Map login reducer state to be accessible via this.props
const mapStateToProps = (state) => {
    return {
        login: state.loginReducer.login,
        error: state.loginReducer.error,
        failed: state.loginReducer.failed,
    }
}

// Caculating screen height
let ScreenHeight = Dimensions.get("window").height;

// The necessary inline styling for the calculated screen height
const Height = StyleSheet.create({ screenheight: {
    height: ScreenHeight,
    } });

// Add props to the view.
export default connect(mapStateToProps, mapDispatchToProps)(Login);
