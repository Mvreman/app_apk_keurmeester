import { DELETE_EXAM, deleteExamError } from './types';
import api from '../../Api.js'

// cancel exam, and send deleted exam to the store (so it can be filtered out of the exams state).
export const cancelExam = (id) => dispatch => {
    api().delete(`exam/${id}`)
        .then(response => dispatch({
            type: DELETE_EXAM,
            payload: response.data
        }))
        .catch(err => {
            dispatch(deleteExamError(err))
            console.log(err, "Delete exam api error");
        });
}