import { FETCH_EXAMS, fetchExamsError } from './types';
import api from '../../Api.js';

// fetch all exams
export const fetchExams = () => dispatch => {
    api().get('exam')
        .then(response => dispatch({
            type: FETCH_EXAMS,
            payload: response.data.exams
        }))
        .catch(err => {
            dispatch(fetchExamsError(err))
            console.log(err, "Get all exams api error")
        });
}

