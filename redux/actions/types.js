// login types
export const POST_LOGINS = 'POST_LOGINS';
export const POST_LOGIN_ERROR = 'POST_LOGIN_ERROR';
export const FETCH_LOGINS = 'FETCH_LOGINS';
export const FETCH_USER = 'FETCH_USER';
export const LOGOUT_USER = 'LOGOUT_USER';


// exam types
export const DELETE_EXAM = 'DELETE_EXAM';
export const DELETE_EXAM_ERROR = 'DELETE_EXAM_ERROR';
export const FETCH_EXAM_PENDING = 'FETCH_EXAM_PENDING';
export const FETCH_EXAM_SUCCESS = 'FETCH_EXAM_SUCCESS';
export const FETCH_EXAM_ERROR = 'FETCH_EXAM_ERROR';
export const UPDATE_EXAM_SUCCESS = 'UPDATE_EXAM_SUCCESS';
export const UPDATE_EXAM_ERROR = 'UPDATE_EXAM_ERROR';
export const CLEAR_EXAM = 'CLEAR_EXAM';
export const FETCH_EXAMS = 'FETCH_EXAMS';
export const FETCH_EXAMS_ERROR = 'FETCH_EXAMS_ERROR';
export const FETCH_EXAMS_HOME_PENDING = 'FETCH_EXAMS_HOME_PENDING';
export const FETCH_EXAMS_HOME_SUCCESS = 'FETCH_EXAMS_HOME_SUCCESS';
export const FETCH_EXAMS_HOME_ERROR = 'FETCH_EXAMS_HOME_ERROR';
export const FETCH_EXAM_PARAMS = 'FETCH_EXAM_PARAMS';
export const FETCH_ALL_EXAM_DATES = 'FETCH_ALL_EXAM_DATES';

// MESSAGES
export const FETCH_MESSAGES = 'FETCH_MESSAGES';
export const FETCH_MESSAGES_PENDING = 'FETCH_MESSAGES_PENDING';
export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS';
export const FETCH_MESSAGES_ERROR = 'FETCH_MESSAGES_ERROR';

// practice exam types
export const CREATE_PRACTICE_EXAM_PENDING = 'CREATE_PRACTICE_EXAM_PENDING';
export const CREATE_PRACTICE_EXAM_SUCCESS = 'CREATE_PRACTICE_EXAM_SUCCESS';
export const CREATE_PRACTICE_EXAM_ERROR = 'CREATE_PRACTICE_EXAM_ERROR';
export const UPDATE_PRACTICE_EXAM_SUCCESS = 'UPDATE_PRACTICE_EXAM_SUCCESS';
export const CLEAR_PRACTICE_EXAM = 'CLEAR_PRACTICE_EXAM';

// PRACTICE EXAMS
export function createPracticeExamPending() {
    return {
        type: CREATE_PRACTICE_EXAM_PENDING
    }
}

export function createPracticeExamSuccess(practiceExam) {
    return {
        type: CREATE_PRACTICE_EXAM_SUCCESS,
        payload: practiceExam
    }
}

export function createPracticeExamError(error) {
    return {
        type: CREATE_PRACTICE_EXAM_ERROR,
        error: error
    }
}

export function clearPracticeExam() {
    return {
        type: CLEAR_PRACTICE_EXAM,
    }
}

export function updatePracticeQuestionSuccess(practiceExam) {
    return {
        type: UPDATE_PRACTICE_EXAM_SUCCESS,
        payload: practiceExam
    }
}

export function updatePracticeQuestionError(error) {
    return {
        type: CREATE_PRACTICE_EXAM_ERROR,
        error: error
    }
}

// EXAMS
export function fetchExamsError(error) {
    return {
        type: FETCH_EXAMS_ERROR,
        error: error,
        updated: false,
        failed: true
    }
}

export function fetchExamParams(examId) {
    return {
        type: FETCH_EXAM_PARAMS,
        payload: examId
    }
}

export function fetchClearExam() {
    return {
        type: CLEAR_EXAM,
    }
}

export function fetchExamPending() {
    return {
        type: FETCH_EXAM_PENDING
    }
}

export function fetchExamSuccess(exam) {
    return {
        type: FETCH_EXAM_SUCCESS,
        payload: exam
    }
}

export function fetchExamError(error) {
    return {
        type: FETCH_EXAM_ERROR,
        error: error,
        updated: false,
        failed: true
    }
}

export function deleteExamError(error) {
    return {
        type: DELETE_EXAM_ERROR,
        error: error,
        failed: true
    }
}

export function updateExamSuccess(exam) {
    return {
        type: UPDATE_EXAM_SUCCESS,
        payload: exam,
        updated: true
    }
}

export function updateExamError(err) {
    return {
        type: UPDATE_EXAM_ERROR,
        error: err,
        updated: false,
        failed: true
    }
}

export function fetchExamsHomePending() {
    return {
        type: FETCH_EXAMS_HOME_PENDING
    }
}

export function fetchExamsHomeSuccess(data) {
    return {
        type: FETCH_EXAMS_HOME_SUCCESS,
        payload: data
    }
}

export function fetchExamsHomeError(error) {
    return {
        type: FETCH_EXAMS_HOME_ERROR,
        error: error,
        failed: true
    }
}

// Messages
export function fetchMessagesPending() {
    return {
        type: FETCH_MESSAGES_PENDING
    }
}

export function fetchMessagesSuccess(data) {
    return {
        type: FETCH_MESSAGES_SUCCESS,
        payload: data
    }
}

export function fetchMessagesError(error) {
    return {
        type: FETCH_MESSAGES_ERROR,
        error: error,
        failed: true
    }
}

// login
export function postLoginError(error) {
    return {
        type: POST_LOGIN_ERROR,
        error: error,
        failed: true
    }
}