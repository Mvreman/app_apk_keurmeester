
export const goToLawSideNav = (side) => (
    {
        type: 'SideNav',
        payload: side,
    }
);


export const goToLawArticle = (article) => (
    {
        type: 'ARTICLE',
        payload: article,
    }
);
