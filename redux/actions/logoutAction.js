import { LOGOUT_USER } from "./types";

// Dispatch LOGOUT_USER to throw away user data.
export const logout = () => {
    return {
        type: LOGOUT_USER,
    }
};