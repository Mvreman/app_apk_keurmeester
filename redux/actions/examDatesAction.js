import { FETCH_ALL_EXAM_DATES } from './types';
import api from '../../Api.js'
import moment from "moment";

// get all exams and return that data to the reducer.

export default function fetchExamDates() {
    return dispatch => {
        api().get('exams/dates')
        .then(function(response) {
            dispatch({
                type: FETCH_ALL_EXAM_DATES,
                payload: response.data.dates.filter(item => moment(item.date).format('YYYY-MM-DD') > moment().format('YYYY-MM-DD'))
            })
        }).catch(function(error){
            console.log(error)
        });
    }
}
