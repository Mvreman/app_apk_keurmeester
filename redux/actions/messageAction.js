import { fetchMessagesPending, fetchMessagesSuccess, fetchMessagesError } from './types';
import api from '../../Api.js'

// get all messages and return that data to the reducer.
export const fetchMessages = () => dispatch => {
    dispatch(fetchMessagesPending())
    api().get('messages')
    .then(response => dispatch(fetchMessagesSuccess(response.data.messages)))
    .catch(err => {
        dispatch(fetchMessagesError(err))
        console.log(err, "Get messages api error");
    });
}

