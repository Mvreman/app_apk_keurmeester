import {
    fetchExamPending,
    fetchExamSuccess,
    fetchExamError
} from './types';
import api from '../../Api.js'

export default function fetchExam(examId) {
    return dispatch => {
        dispatch(fetchExamPending());
        api().get(`exam/${examId}`)
            .then(res => {
                dispatch(fetchExamSuccess(res.data.exam));
            })
            .catch(err => {
                dispatch(fetchExamError(err))
                console.log(err, "Get one exam api error")
            })
    }
}
