import { fetchExamsHomePending, fetchExamsHomeSuccess, fetchExamsHomeError } from "./types";
import api from "../../Api";

// get all exams for the homescreen and return that data to the reducer.
export const fetchExamsHome = () => dispatch => {
    dispatch(fetchExamsHomePending());
    api().get('exam')
        .then(response => {
            dispatch(fetchExamsHomeSuccess(response.data.exams))
        })
        .catch(err => {
            dispatch(fetchExamsHomeError(err))
            console.log(err, "Fetch home exams api error");
        });
};
