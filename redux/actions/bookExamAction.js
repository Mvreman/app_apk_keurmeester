// add values to the store with exam that is eventually going to be sent to the api
export const bookExam = (values) => {
    return {
        type: 'BOOK_EXAM',
        payload: values,
    }
}

export const bookExamLoading = (values) => {
    return {
        type: 'BOOK_EXAM_PENDING',
    }
}