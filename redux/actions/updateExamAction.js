import { updateExamSuccess,  updateExamError} from './types';
import api from '../../Api.js'

export default function updateExam(examId, body) {
    return dispatch => {
        api().put(`/exam/${examId}`, body)
        .then(response => {
            dispatch(updateExamSuccess(response.data.updatedExam));
        })
        .catch(err => {
            dispatch(updateExamError(err))
            console.log(err, "Update exam api error!")
        }) 
    }
}
