import { POST_LOGINS } from './types';
import api from '../../Api.js'

/**
 * Redux action to update the current user and update the Redux store.
 * 
 * @param user
 */
export const updateUser = (user) => dispatch => {
    api().put("user", user)
        .then(response => (dispatch({
            type: POST_LOGINS,
            payload: response.data.user
        })))
}