import {
    updatePracticeQuestionError,
    updatePracticeQuestionSuccess
} from '../types';
import api from '../../../Api.js'

/**
 * Update API call for the answer given for a practice question
 *
 * @param practiceExamId
 * @param practiceQuestionId
 * @param body
 * @returns {function(...[*]=)}
 */
function updatePracticeQuestion(practiceExamId, practiceQuestionId, body) {
    let question_result = body.correct === 1 ? 1 : 0;

    const bodyParameters = {
        question_result: question_result,
        answer_id: body.id
    };

    return dispatch => {
        api().put(`practiceExam/${practiceExamId}/practiceQuestions/${practiceQuestionId}`, bodyParameters)
            .then(response => {
                dispatch(updatePracticeQuestionSuccess(response.data.updatedPracticeQuestion));
            }).catch(error => {
                dispatch(updatePracticeQuestionError(error))
        })
    }
}

export default updatePracticeQuestion
