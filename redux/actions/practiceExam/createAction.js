import {
    createPracticeExamPending,
    createPracticeExamSuccess,
    createPracticeExamError,
} from '../types';
import api from '../../../Api.js'

/**
 * Create API call for practice exam
 *
 * @param exam_type
 * @returns {function(...[*]=)}
 */
export default function createPracticeExam(exam_type) {
    const body = {
        exam_type: exam_type
    };

    return dispatch => {
        dispatch(createPracticeExamPending());

        api().post('/practiceExam', body)
            .then(response =>
            {
                dispatch(createPracticeExamSuccess(response.data.practiceExam))
            }).catch(error => {
                console.log(error);
                createPracticeExamError(error)
        });
    }
}
