// add new created exam to exams state in reducer
export const addExam = (exam) => {
    return {
        type: 'ADD_EXAM',
        payload: exam,
    }
}

export const ClearExamState = () => {
    return {
        type: 'CLEAR_EXAM_STATE',
    }
}
