import { POST_LOGINS, postLoginError } from "./types";
import api from '../../Api.js'

// Sends user data to API call for login. TODO: IMPLEMENT RETURNING RESPONSE MESSAGE TO APP
export const postLogins = (bodyParameters) => {
    return async dispatch => {
        return await api().post('login', bodyParameters)
            .then(response => dispatch({
                type: POST_LOGINS,
                payload: response.data.user
            }))
            .catch(err => {
                dispatch(postLoginError(err))
                console.log(err, "Post login api error");
            });
    }
}