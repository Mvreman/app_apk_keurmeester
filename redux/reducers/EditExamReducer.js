import { 
    FETCH_EXAM_SUCCESS, 
    FETCH_EXAM_PENDING, 
    FETCH_EXAM_ERROR,
    UPDATE_EXAM_SUCCESS,
    FETCH_EXAM_PARAMS,
    CLEAR_EXAM,
    UPDATE_EXAM_ERROR
} from '../actions/types.js';

const initialState = {
    pending: false,
    exam: [],
    error: null,
    updated: false,
    failed: false,
    examId: null
}

export default function (state = initialState, action) {
    switch(action.type) {
        case FETCH_EXAM_PARAMS:
            return {
                ...state,
                pending: true,
                examId: action.payload
            }
        case FETCH_EXAM_PENDING: 
            return {
                ...state,
                pending: true
            }
        case FETCH_EXAM_SUCCESS:
            return {
                ...state,
                pending: false,
                exam: action.payload
            }
        case FETCH_EXAM_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
                failed: action.failed
            }
        case UPDATE_EXAM_SUCCESS:
            return {
                ...state,
                pending:false,
                exam:action.payload,
                updated: action.updated
            }
        case UPDATE_EXAM_ERROR: 
            return {
                ...state,
                pending: false,
                error: action.error,
                failed: action.failed
            }
        case CLEAR_EXAM:
            return initialState;
        default:
            return state;
    } 
}