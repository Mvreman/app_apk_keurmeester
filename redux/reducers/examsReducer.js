import { FETCH_EXAMS, DELETE_EXAM, DELETE_EXAM_ERROR, FETCH_EXAMS_ERROR } from '../actions/types.js';

const initialState = {
    bookExam: {},
    exams: [],
    postExam: {},
    loading: false,
    error: null,
    failed: false
}

/**
 * reducer that makes a new state for all the exam related topics
 */
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_EXAMS:
            return {
                ...state,
                exams: action.payload
            }
        case FETCH_EXAMS_ERROR:
            return {
                ...state,
                error: action.error,
                failed: action.failed,
                loading: false,
            }
        case 'BOOK_EXAM':
            return {
                ...state,
                bookExam: { ...state.bookExam, ...action.payload }
            }
        case 'BOOK_EXAM_PENDING':
            return {
                ...state,
                loading: true,
            }
        case 'ADD_EXAM':
            return {
                ...state,
                exams: [...state.exams, action.payload],
            }
        case 'CLEAR_EXAM_STATE':
            return {
                ...state,
                bookExam: [],
            }
        case DELETE_EXAM:
            return {
                ...state,
                exams: state.exams.map((exam, index) => {
                    if (exam.id === action.payload.deletedExam.id) {
                        return action.payload.deletedExam
                    } else {
                        return exam
                    }
                })
            }
        case DELETE_EXAM_ERROR:
            return {
                ...state,
                error: action.error,
                failed: action.failed
            }
        default:
            return state;

    }
}
