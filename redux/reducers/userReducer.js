import { FETCH_USER_INFO } from '../actions/types.js';

const initialState = {
    user: {},
}

export default function (state = initialState, action) {
    switch(action.type) {
        case "FETCH_USER_INFO":
            return {
                ...state,
                user: action.payload
            }
        default:
            return state;
    } 
}