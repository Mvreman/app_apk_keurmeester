import { FETCH_ALL_EXAM_DATES } from "../actions/types";

const initialState = {
    dates: [],
}

// Fill the initialState with all the exams.
export default function (state = initialState, action) {
    switch(action.type) {
        case FETCH_ALL_EXAM_DATES:
            return {
                ...state,
                dates: action.payload
            }
        default:
            return state;
    }
}