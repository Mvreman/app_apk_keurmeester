const authsReducer = (state = {}, action) => {
    switch(action.type) {
        case 'ADD_USER':
            return state + action.payload;
        case 'REMOVE_USER':
            state = '';
            return state;
        default:
            return state;
    }
}

export default authsReducer;