import {
    CREATE_PRACTICE_EXAM_SUCCESS,
    CREATE_PRACTICE_EXAM_PENDING,
    CREATE_PRACTICE_EXAM_ERROR,
    UPDATE_PRACTICE_EXAM_SUCCESS,
    CLEAR_PRACTICE_EXAM
} from '../actions/types.js';

const initialState = {
    pending: false,
    practiceExam: [],
    storedPracticeExam: [],
    error: null,
    failed: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case CREATE_PRACTICE_EXAM_PENDING:
            return {
                ...state,
                pending: true
            };
        case CREATE_PRACTICE_EXAM_SUCCESS:
            return {
                ...state,
                pending: false,
                practiceExam: action.payload
            };
        case CREATE_PRACTICE_EXAM_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
                failed: true
            };
        case UPDATE_PRACTICE_EXAM_SUCCESS: {
            return {
                ...state,
                pending: false,
                updatedPracticeQuestion: action.payload
            }
        }
        case CLEAR_PRACTICE_EXAM:
            return {
                practiceExam: []
            };
        default:
            return state;
    }
}
