import {
    FETCH_EXAMS_HOME_PENDING,
    FETCH_EXAMS_HOME_SUCCESS,
    FETCH_EXAMS_HOME_ERROR
} from "../actions/types";

const initialState = {
    exams: [],
    homeExamsPending: false,
    error: null,
    failed: false
};

/**
 * Fill the initialState with all the exams for the homescreen
 *
 * @param state
 * @param action
 * @returns {{homeExamsPending: boolean, exams: [], failed: boolean, error: null}|{homeExamsPending: boolean, exams: *, failed: boolean, error: null}|{homeExamsPending: boolean, exams: [], failed: *, error: *}}
 */
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_EXAMS_HOME_PENDING:
            return {
                ...state,
                homeExamsPending: true,
            }
        case FETCH_EXAMS_HOME_SUCCESS:
            return {
                ...state,
                homeExamsPending: false,
                exams: action.payload
            }
        case FETCH_EXAMS_HOME_ERROR:
            return {
                ...state,
                homeExamsPending: false,
                error: action.error,
                failed: action.failed,
            }
        default:
            return state;
    }
}
