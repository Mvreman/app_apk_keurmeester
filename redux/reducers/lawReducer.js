import { ARTICLE } from '../actions/types.js';

const initialState = {
    article: "",
    sideNav: "",
};

export default function (state = initialState, action) {
    switch(action.type) {
        case "SideNav":
            return {
                ...state,
                sideNav: action.payload
            };
        case "ARTICLE":
            return {
                ...state,
                article: action.payload
            };
        default:
            return state;
    }
}
