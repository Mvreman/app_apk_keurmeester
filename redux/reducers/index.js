import { combineReducers } from 'redux';
import authsReducer from './authsReducer.js';
import loginReducer from './loginsReducer.js';
import examsReducer from './examsReducer.js';

import EditExamReducer from './EditExamReducer';
import practiceExamsReducer from "./practiceExamsReducer";
import fetchAllExameDates from "./examDatesReducer";
import lawReducer from "./lawReducer";
import messageReducer from "./messageReducer";
import examsHomeReducer from "./examsHomeReducer";

export default combineReducers({
    authsReducer: authsReducer,
    loginReducer: loginReducer,
    examsReducer: examsReducer,
    EditExamReducer: EditExamReducer,
    practiceExamReducer: practiceExamsReducer,
    fetchAllExameDates: fetchAllExameDates,
    lawReducer: lawReducer,
    messageReducer: messageReducer,
    examsHomeReducer: examsHomeReducer,
});
