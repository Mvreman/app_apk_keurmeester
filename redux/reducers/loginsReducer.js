import { FETCH_LOGINS, POST_LOGINS, LOGOUT_USER, POST_LOGIN_ERROR } from "../actions/types.js";

const initialState = {
    login: [],
    error: null,
    failed: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_LOGINS: {
            return {
                state,
            };
        }
        case POST_LOGINS: {
            return {
                ...state,
                login: action.payload,
            };
        }
        case POST_LOGIN_ERROR: {
            return {
                ...state,
                error: action.error,
                failed: action.failed,
            };
        }
        case LOGOUT_USER: {
            return {
                ...state,
                login: [],
            };
        }
        default:
            return state;
    }
}
