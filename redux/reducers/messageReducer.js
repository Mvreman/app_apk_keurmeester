import { FETCH_MESSAGES_PENDING, FETCH_MESSAGES_SUCCESS, FETCH_MESSAGES_ERROR } from "../actions/types";

const initialState = {
    messages: [],
    pending: false,
    error: null,
    failed: false
};

/**
 * Fill the initialState with all the messages
 *
 * @param state
 * @param action
 * @returns {{pending: boolean, messages: [], failed: boolean, error: null}|{pending: boolean, messages: [], failed: *, error: *}|{pending: boolean, messages: *, failed: boolean, error: null}}
 */
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_MESSAGES_PENDING:
            return {
                ...state,
                pending: true,
            }
        case FETCH_MESSAGES_SUCCESS:
            return {
                ...state,
                pending: false,
                messages: action.payload
            }
        case FETCH_MESSAGES_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
                failed: action.failed,
            }
        default:
            return state;
    }
}
