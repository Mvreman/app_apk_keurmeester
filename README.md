# App APK-Keurmeester
This application is commissioned by IBKI.

## Installation
#### Installing npm and Expo
* Install [NodeJs](https://nodejs.org/en/download/)
* Open the project and run in the command line: 
```bash
npm install
```
* Run the following command in the command line:
```bash
npm install expo-cli --global
```

## Usage
There are two ways to run this application.

#### Using your mobile device
* Download the Expo app for [Android](https://play.google.com/store/apps/details?id=host.exp.exponent&referrer=www) or [iOS](https://apps.apple.com/app/apple-store/id982107779)
* Open the application on your mobile device
* Run the following command in the command line of the project:
```bash
expo start
```
* A new tab inside your default browser will open
* Scan the QR Code with the QR Code scanner within the Expo app on your mobile device
* Once the Expo app says the Javascript bundle is build, the application will open up on your device

#### Using Android Emulator
* Download [Android Studio](https://developer.android.com/studio) 
* To create and manage an Android Virtual Device, follow the steps in this [link](https://developer.android.com/studio/run/managing-avds)
* Run the following command in the command line of the project:
```bash
expo start
```
* A new tab inside your default browser will open
* Click on ```Run on Android device/emulator```
* Once the Expo app says the Javascript bundle is build, the application will open up on your Android Virtual Device

## Authors
Jasper Agelink | Jonathan Bregman | Mike Bulte | Bart Burgman | Tycho Doorman | Marco Lohmeijer | Nick Meulenbroek | Annemarieke van Veen | Menno Vreman 

&copy; Copyright Team Codesign