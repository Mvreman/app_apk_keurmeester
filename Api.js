import axios from 'axios';
import store from "./redux/store";

/**
 * When this function gets called, return an axios instance. 
 * Return an instance with bearer token if the user is logged in.
 */
export default function() {
    const auth = store.getState().loginReducer.login;

    const instance = axios.create({
        baseURL: 'https://adsd2019.clow.nl/~marco/999games/api',
    });

    if(auth.api_token) {
        instance.defaults.headers.common['Authorization'] = `Bearer ${auth.api_token}`;
    }

    return instance
}
