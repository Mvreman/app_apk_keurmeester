import {StyleSheet} from "react-native";

export default styles = StyleSheet.create({
    container: {
        borderWidth: 0,
        padding: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 3,
            height: 0,
        },
        shadowOpacity: 0.3,
        shadowRadius: 5,

        elevation: 7,
        marginHorizontal: 5,
        marginVertical: 10,
    },
    selected: {
        borderWidth: 1,
        borderColor: "blue",
        padding: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 3,
            height: 0,
        },
        shadowOpacity: 0.3,
        shadowRadius: 5,

        elevation: 7,
        marginHorizontal: 5,
        marginVertical: 10,
    },
})
