import {Dimensions, StyleSheet} from "react-native";

const ScreenHeight = Dimensions.get('window').height;

export default styles = StyleSheet.create({
    // General
    floatRight: {
        alignSelf: 'flex-end',
    },
    marginTop: {
      marginTop: 20
    },

    stretch:{
        alignSelf: 'stretch'
    },
    spacing:{
        marginBottom: 50,
        alignSelf: 'stretch',
    },

    // Container
    bottomView: {
        flex: 0.1,
        width: '100%',
        height: 50,
        backgroundColor: "#FDE803",
        justifyContent: 'center',
        alignItems: 'center',
    },

    fixCard:{
        padding: 0,
        alignSelf: 'stretch',
    },

    // Backgrounds
    bgYellow: {
        backgroundColor: '#FDE803',
        padding: 10,
        paddingLeft: 20,
    },

    bgRed: {
        backgroundColor: '#C93636'
    },

    bgBlue: {
        backgroundColor: '#133B96'
    },

    // Text
    titleText: {
        fontSize: 22,
        fontWeight: "bold",
        marginBottom: 0,
        flex: 0.3
    },
    textWhite: {
        color: '#FFFFFF',
        alignSelf: "center"
    },

    // Buttons
    btnBackToExam: {
        width: 200,
        alignSelf: "flex-start",
        alignItems: "center",
        paddingVertical: 10,
        position: "absolute"
    },
    btnFinishExam : {
        width: 150,
        alignSelf: 'flex-end',
        alignItems: "center",
        paddingVertical: 10,
        position: "absolute"
    },
    btnExamType: {
        padding: 10,
        width: "45%",
        alignItems: "center",
        backgroundColor: "#133B96",
        justifyContent: "center",
        marginVertical: 10
    },

    btnSideMenu: {
        width: 50,
        height: 50,
        alignSelf: "flex-end",
        alignItems: "center",
        backgroundColor: "#133B96",
        justifyContent: "center"
    },
    btnText: {
        color: "#FFFFFF"
    },
    btnPrevious: {
        width: 200,
        alignSelf: "flex-start",
        alignItems: "center",
        paddingVertical: 10,
        position: "absolute"
    },
    btnNext: {
        width: 200,
        alignSelf: 'flex-end',
        alignItems: "center",
        paddingVertical: 10,
        position: "absolute"
    },

    // Points for show practice exam
    points: {
        fontSize: 12,
        color: '#133B96',
        paddingLeft: 10,
        top: 13,
        flex: 0.65
    },
})
