import { StyleSheet } from "react-native";

export default styles = StyleSheet.create({
    messageCard: {
        flex: 1,
        justifyContent: "center",
    },
    messageCardInner: {
        alignSelf: 'stretch',
        marginHorizontal: 10,
    },
    messageArrow: {
        position: 'absolute',
        right: 10,
        top: -5,
    }
})

