import { StyleSheet } from "react-native";

export default styles = StyleSheet.create({


    dateInput:{
        height: 40,
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderColor: '#000000',
        borderWidth: 0.5,
        marginBottom: 10,
        width: "100%",
        backgroundColor: '#ffffff',
        shadowColor: "#000",
        shadowOffset: {
            width: 3,
            height: 0,
        },
        shadowOpacity: 0.3,
        shadowRadius: 5,

    }

})
