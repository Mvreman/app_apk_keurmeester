import {Dimensions} from "react-native";
import {StyleSheet} from "react-native";
import {responsiveFontSize, responsiveScreenFontSize, responsiveScreenHeight} from "react-native-responsive-dimensions";

const ScreenHeight = Dimensions.get('window').height;

export default styles = StyleSheet.create({
    container_card:{
            marginLeft: 30,
            marginRight: 20,
    },

    viewSpacing:{
        marginHorizontal: 20,
        marginTop: 15,

    },
    dashHorizontal: {
        height:1,
        marginVertical: 20 ,
    },
    dashVertical: {
        width:2,
        height: ScreenHeight,
        marginTop: 10,
        flexDirection:'column',
    },

    dashContainer:{
        position: "absolute",
        marginTop: 50,
        paddingLeft: 20,
    },

    flatList: {
        paddingBottom: 10,
        marginLeft:14,
    },

    dot: {
        width: 14,
        height: 14,
        top: '43%',
        borderRadius: 100/2,
        backgroundColor: '#FDE803'
    },

    card: {
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 15,
        width: '100%',
        backgroundColor: '#fff',
        alignSelf: 'flex-start',
        shadowColor: "#000",
        shadowOffset: {
            width: 3,
            height: 0,
        },
        shadowOpacity: 0.3,
        shadowRadius: 5,

        elevation: 7,
        marginBottom: 10,
    },

    // examen-styles
    plannedExams:{
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap'
    },

    descriptionGeplandeExamens: {
        flex: 1,
        fontSize: 12
    },

    btnAdd: {
        width: 30,
        height: 30,
        position: 'absolute',
        alignSelf: "flex-end",
        alignItems: "center",
        backgroundColor: "#133B96",
        justifyContent: "center"
    },

    type:{
        fontSize: 12,
        fontWeight: 'bold',
        marginBottom: 4
    },

    dateGeplandeExamens: {
        flex: 0.2,
        flexDirection: 'column',
        marginRight: 8,
        alignItems: 'center',
    },

    arrow:{
        flex: 0.1
    },

    spacingExamsDetail:{
        marginHorizontal: 10
    },
    blueBtn:{
        backgroundColor: "#fff",
        marginTop: 0,
        marginHorizontal: responsiveScreenHeight(0)
    }
})


