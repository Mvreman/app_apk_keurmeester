import General from './generalStyle';
import Home from './homeStyle';
import ThirdExame from './thirdAddExamesStyle';
import SecondExame from './secondAddExamesStyle';
import Regelgeving from "./regelgevingStyle";
import Profile from './profileStyle';
import Auth from './authStyling';
import Button from './buttonStyling';
import ShowAllExams from './showAllExamsStyle'
import Messages from "./messagesStyle";
import Modal from './modalStyling';
import PracticeExam from './practiceExamStyle';
import Exames from './examesStyling'

export {
    General,
    Home,
    SecondExame,
    ThirdExame,
    Regelgeving,
    Profile,
    Auth,
    Exames,
    Button,
    ShowAllExams,
    Modal,
    Messages,
    PracticeExam,
}
