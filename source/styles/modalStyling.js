import { StyleSheet } from "react-native";

export default styles = StyleSheet.create({
    Modal: {
        alignItems: 'center',
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15
    },
    Text: {
        marginBottom: 15,
        fontSize: 17,
    },
    Buttonholder: {
        alignSelf: 'stretch',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    Button: {
        marginRight: 10,
    }
});