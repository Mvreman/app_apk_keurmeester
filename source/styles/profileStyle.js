import {StyleSheet} from "react-native";

export default styles = StyleSheet.create({
    formField:{
        marginTop: 15,
    },
    formFieldLabel:{
        fontWeight: 'bold',
        marginBottom: 10
    },
    formFieldInput:{
        height: 40,
        backgroundColor: '#fff',
        paddingHorizontal: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 3,
            height: 0,
        },
        shadowOpacity: 0.3,
        shadowRadius: 5,

        elevation: 7,
    }
})
