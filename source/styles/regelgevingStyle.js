import {StyleSheet} from "react-native";

export default styles = StyleSheet.create({

    dropdown:{
        zIndex: 9999,
        marginTop: '5%',
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
    },

    image:{
        marginVertical: "8%",
        width: "100%",
        height: 200,
        resizeMode: "contain",
    },

    icon:{
        width: 20,
        height: 20,
        resizeMode: "contain",
    },

    part:{
        width: 120,
        marginHorizontal: 5,
        marginVertical: '2%',
        padding: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
        alignItems: 'center'
    },

    box_links:{
        marginVertical: 20,
        padding: 15,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
    },

    center: {
        alignItems: 'center'
    },

    font: {
        fontFamily: 'Roboto-slab',
        fontSize: 24
    },

    fixCard:{
        flex: 0,
        padding: 0,
        alignSelf: 'stretch',
    },

    insideCard: {
        padding: 20,
    },

    cardHead:{
        marginRight: "3%",
        marginBottom: "2%",
    },

    bottom:{
        marginBottom: 10
    }
})
