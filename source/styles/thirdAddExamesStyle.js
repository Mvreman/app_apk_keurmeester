import {StyleSheet} from "react-native";

export default styles = StyleSheet.create({
    input: {
        color: "grey",
        padding: 10,
        fontSize: 18,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 3,
            height: 0,
        },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        elevation: 7,
        marginVertical: 10,
    },
    link: {
        color: "blue",
        borderWidth: 0,
        paddingHorizontal: 20,
        padding: 10,
        fontSize: 16,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 3,
            height: 0,
        },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        elevation: 7,
        marginBottom: 20
    },
    bold: {
        fontWeight: 'bold',
        fontSize: 16,
        fontFamily: 'Roboto-slab-bold',
    },
    errorDisplay: {
        fontWeight: '400',
        fontSize: 10,
        marginTop: -7,
        color: '#d9534f',
    },

    bottom:{
        marginBottom: 20
    },

    checkbox:{
        margin: 0,
        marginLeft: 0,
        marginRight: 0,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: "white"
    }
})
