import { StyleSheet } from "react-native";
import {responsiveFontSize, responsiveScreenFontSize, responsiveScreenHeight} from "react-native-responsive-dimensions";

export default styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 20,
        fontFamily: "roboto",
    },
    column: {
        flex: 1,
        margin: 20,
        flexDirection: 'column'
    },
    row: {
        justifyContent: 'center',
        flexDirection: 'row',
    },
    header: {
        width: '100%',
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerText: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#000',
        letterSpacing: 1,
    },
    errorText: {
        color: 'crimson',
        fontWeight: 'bold',
        marginBottom: 10,
        marginTop: 6,
        textAlign: 'center',
    },
    menuBtn: {
        position: 'absolute',
        left: 0,
    },
    input: {
        borderWidth: 1,
        padding: 10,
        fontSize: 18,
        margin: 5,
    },
    h1: {
        fontSize: responsiveFontSize(3),
        fontFamily: 'Roboto-slab-bold',
        marginBottom: 10,
    },
    h2: {
        fontSize: responsiveFontSize(2.5),
        fontFamily: 'Roboto-slab-bold',
        marginBottom: 10,
    },
    h3: {
        fontSize: 28
    },
    h4: {
        fontSize: 24
    },
    h5: {
        fontSize: 20
    },
    h6: {
        fontSize: 16
    },
    title: {
        fontWeight: 'bold'
    },
    text:{
        fontSize: responsiveScreenFontSize(1.5),
    },
    gradient: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        height: 600,
        opacity: 0.3
    },
    viewSpacing: {
        marginBottom: responsiveScreenHeight(5),
    },
    card: {
        flex: 1,
        padding: 20,
        backgroundColor: '#fff',
        alignSelf: 'flex-start',
        shadowColor: "#000",
        shadowOffset: {
            width: 3,
            height: 0,
        },
        shadowOpacity: 0.3,
        shadowRadius: 5,

        elevation: 7,
        marginVertical: 10,
    },
    footer: {
        height: responsiveScreenHeight(7),
        backgroundColor: '#FDE803',
    },
    dropdown: {
        marginBottom: 10,
        height: 35,
    },
    cardRow: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        height: 'auto',
    },
    button: {
        backgroundColor: "#133B96",
        padding: 10,
        margin: 2,
    },
    buttonText: {
        color: '#FFF',

    },
    margin0: {
        margin: 0,
    }
})

