import { StyleSheet } from "react-native";

export default styles = StyleSheet.create({
    loginScrollView: {
        flex: 1,
        height: "100%",
    },
    backgroundImage: {
        flex: 1,
    },
    loginContainer: {
        flex: 1,
        fontFamily: "roboto",
        marginHorizontal: 20,
        justifyContent: "flex-end",
    },
    alignment: {
        flex: 1,
    },
    logo: {
        width: '100%',
        resizeMode: 'contain',
    },
    inputContainer: {
        padding: 20,
        backgroundColor: '#fff',
    },
    password: {
        backgroundColor: '#fff',
        padding: 15,
        color: '#000',
        borderWidth: 1,
        borderColor: '#000',
    },
    input: {
        backgroundColor: '#fff',
        padding: 15,
        color: '#000',
        borderWidth: 1,
        borderColor: '#000',
    },
})
