import { StyleSheet } from "react-native";
import  {responsiveFontSize, responsiveScreenFontSize, responsiveScreenHeight} from "react-native-responsive-dimensions";

export default styles = StyleSheet.create({
    topBoxes: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'stretch',
        height: 'auto',
    },
    topBoxLeft: {
        marginRight: "2%",
        paddingVertical: 30,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'stretch',
        height: responsiveScreenHeight(18),
    },
    topBoxRight: {
        marginLeft: "2%",
        paddingVertical: 30,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'stretch',
        height: responsiveScreenHeight(18),
    },
    plannedExams: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap'
    },
    datePlannedExams: {
        flex: 0.2,
        flexDirection: 'column',
        marginRight: 10,
        alignItems: 'center',
    },
    descriptionPlannedExams: {
        flex: 1,
    },
    show: {
      fontWeight: 'bold',
      textAlign: 'right',
    },
    messageBox: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        backgroundColor: '#fff',
        alignSelf: 'stretch',
        shadowColor: "#000",
        shadowOffset: {
            width: 3,
            height: 0,
        },
        shadowOpacity: 0.3,
        shadowRadius: 5,

        elevation: 7,
        marginVertical: 10,
        marginHorizontal: 5,
    },
    message: {
        flex: 10,
        padding: 20,
    },
    messageInfo: {

    },
    messageBtn: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FDE803',
        padding: 20,
    },
    homePractice: {
        backgroundColor: '#FDE803',
        padding: 30,
        alignSelf: 'flex-start',
        shadowColor: "#000",
        shadowOffset: {
            width: 3,
            height: 0,
        },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        elevation: 7,
        marginVertical: 10,
        justifyContent: 'center',
        alignItems: 'center',
        width: "100%"
    },
    homePracBtns: {
        flexDirection: 'row',
    },
        backgroundVideo: {
            position: 'absolute',
            top: 0,
            left: 0,
            bottom: 0,
            right: 0,
        },
})
