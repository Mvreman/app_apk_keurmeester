import {StyleSheet} from "react-native";
import {responsiveScreenFontSize} from "react-native-responsive-dimensions";

export default styles = StyleSheet.create({
    textStyle: {
        fontSize: responsiveScreenFontSize(2),
        color: '#ffffff',
        textAlign: 'center'
    },

    textStyleSpecial: {
        fontSize: 18,
        color: '#000',
        textAlign: 'center',
        fontWeight: 'bold',
    },

    greyStyle: {
        padding: 10,
        backgroundColor: '#E5E5E5',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },

    blueStyle: {
        padding: 10,
        backgroundColor: '#133B96',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 10,
        marginTop: 20,
    },

    yellowStyle: {
        padding: 10,
        backgroundColor: '#FFE701',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },

    redStyle: {
        padding:10,
        paddingHorizontal: 15,
        backgroundColor: '#CC3300',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
})
