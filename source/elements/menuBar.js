import React from 'react';
import { Text, View } from 'react-native';
import { MaterialIcons } from "@expo/vector-icons";
import {Style} from "../index";

export default function MenuBar({ navigation, title }) {

    const openMenu = () => {
        navigation.openDrawer()
    }

    return (
        <View style={Style.General.header}>
            <MaterialIcons name={'menu'} size={28} onPress={openMenu} style={Style.General.menuBtn} />
            <View>
                <Text style={Style.General.headerText}>{ title }</Text>
            </View>
        </View>
    );
}
