import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, Text, Image} from 'react-native';
import { Style } from "../../source";

class RDWButton extends Component {
    render() {
        const { text, onPress} = this.props;
        return (
            <TouchableOpacity style={Style.Button.redStyle}
                              onPress={() => onPress()}
            >
                <Image
                    source={require('../../assets/logo-rdw.png')}
                    style={{ width: 30, height: 30, marginRight: 10 }}
                />
                <Text style={Style.Button.textStyle}>{text}</Text>
            </TouchableOpacity>
        );
    }
}

RDWButton.propTypes = {
    text: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired
};

export default RDWButton;
