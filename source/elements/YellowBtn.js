import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, Text } from 'react-native';
import { Style } from "../../source";

class YellowButton extends Component {
    render() {
        const { text, onPress} = this.props;
        return (
            <TouchableOpacity style={Style.Button.yellowStyle}
                              onPress={() => onPress()}
            >
                <Text style={Style.Button.textStyleSpecial}>{text}</Text>
            </TouchableOpacity>
        );
    }
}

YellowButton.propTypes = {
    text: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired
};

export default YellowButton;
