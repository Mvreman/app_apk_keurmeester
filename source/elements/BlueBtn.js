import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, Text } from 'react-native';
import { Style } from "../../source";

class BlueButton extends Component {
    render() {
        const { text, onPress} = this.props;
        return (
            <TouchableOpacity style={Style.Button.blueStyle}
                              onPress={() => onPress()}>
                <Text style={Style.Button.textStyle}>{text}</Text>
            </TouchableOpacity>
        );
    }
}

BlueButton.propTypes = {
    text: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired
};

export default BlueButton;
