import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';
import { Octicons } from '@expo/vector-icons';

class regulationsBTN extends Component {
    render() {
        const { text, onPress} = this.props;
        return (
            <TouchableOpacity style={styles.buttonStyle}
                              onPress={() => onPress()}>
                <Octicons name="primitive-dot" size={16} color="#133B96"  />
                <Text style={styles.textStyle}>{text}</Text>
            </TouchableOpacity>
        );
    }
}

regulationsBTN.propTypes = {
    text: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
    textStyle: {
        fontSize: 14,
        color: '#133B96',
        textAlign: 'left',
        paddingLeft: 15
    },

    buttonStyle: {

        padding:10,
        flexDirection: 'row',

    },
    dot:{
        padding:10
    }
});

export default regulationsBTN;
