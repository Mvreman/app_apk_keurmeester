import React, { Component } from 'react';
import { Text, Image, View, ScrollView } from 'react-native';
import { Style } from "../../index";

class PAuto_Front extends Component {
    render() {
        return (
            <View>
                <Image source={require('../../../assets/regelgeving/personenauto.png')}
                       style={Style.Regelgeving.image}/>
                <View style={Style.Regelgeving.center}>
                    <Text style={[Style.General.h2, Style.Regelgeving.font]} >Voorkant</Text>
                </View>
                <View>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={true} pagingEnabled={true}>
                        <View style={Style.Regelgeving.part}>
                            <Image source={require('../../../assets/regelgeving/icon/car.png')}
                                   style={Style.Regelgeving.icon}/>
                               <Text>Carrossie</Text>
                        </View>
                        <View style={Style.Regelgeving.part}>
                            <Image source={require('../../../assets/regelgeving/icon/glass.png')}
                                   style={Style.Regelgeving.icon}/>
                            <Text>Ruiten</Text>
                        </View>
                        <View style={Style.Regelgeving.part}>
                            <Image source={require('../../../assets/regelgeving/icon/motor.png')}
                                   style={Style.Regelgeving.icon}/>
                            <Text>Motor</Text>
                        </View>
                        <View style={Style.Regelgeving.part}>
                            <Image source={require('../../../assets/regelgeving/icon/lights.png')}
                                   style={Style.Regelgeving.icon}/>
                            <Text>Lichten</Text>
                        </View>
                        <View style={Style.Regelgeving.part}>
                            <Image source={require('../../../assets/regelgeving/icon/mirror.png')}
                                   style={Style.Regelgeving.icon}/>
                            <Text>Spiegels</Text>
                        </View>
                        <View style={Style.Regelgeving.part}>
                            <Text>Identificatie</Text>
                        </View>
                        <View style={Style.Regelgeving.part}>
                            <Text>Banden</Text>
                        </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
}

export default PAuto_Front;
