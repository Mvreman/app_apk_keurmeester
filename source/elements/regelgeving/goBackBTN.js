import React, {Component} from 'react';
import {TouchableOpacity, Text, StyleSheet, View} from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import PropTypes from "prop-types";

class goBackBTN extends Component {
    render() {
        const { onPress} = this.props;
        return (
            <View style={styles.bg}>
                <TouchableOpacity style={styles.buttonStyle} onPress={() => onPress()}>
                    <AntDesign style={styles.arrow} name="arrowleft" size={20} color="black"/>
                    <Text style={styles.textStyle}>Terug naar overzicht</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

goBackBTN.propTypes = {
    onPress: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
    textStyle: {
        flex:0.8,
        fontSize: 16,
        fontWeight: 'bold',
        color: 'black',
    },

    buttonStyle: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap',

    },

    arrow: {
        flex:0.15
    },

    bg:{
        backgroundColor: '#FDE803',
        paddingHorizontal: 20,
        paddingVertical: 15,
        height: 50
    }
});

export default goBackBTN;
