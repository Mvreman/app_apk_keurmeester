import RDWButton from "./RDWBtn";
import MenuBar from "./menuBar";
import BlueButton from "./BlueBtn";
import YellowButton from "./YellowBtn";
import RegulationsBTN from "./regelgeving/regulationsBTN";
import PAuto_Front from "./regelgeving/PAuto_Front";
import GoBackBTN from "./regelgeving/goBackBTN";

export {
    MenuBar,
    RDWButton,
    BlueButton,
    YellowButton,
    RegulationsBTN,
    PAuto_Front,
    GoBackBTN,
}
