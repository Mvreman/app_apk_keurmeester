import { createStackNavigator } from "@react-navigation/stack";
import {ShowAllExams} from "../screens";
import React from "react";
import MenuBar from "../source/elements/menuBar";

/**
 * Stack for showing all exams screen
 *
 * @type {import("@react-navigation/native").TypedNavigator<Record<string, object | undefined>, StackNavigationState, StackNavigationOptions, StackNavigationEventMap, typeof StackNavigator>}
 */
const ShowAllExamsStack = createStackNavigator();
const ShowAllExamsStackScreen = () => (
    <ShowAllExamsStack.Navigator>
        <ShowAllExamsStack.Screen
            name={"ShowallExams"}
            component={ShowAllExams}
            options={ ({ navigation }) => {
                return {
                    headerTitle: () => <MenuBar navigation={navigation} title={'Mijn Examens'} />,
                    headerStyle: {
                        backgroundColor: '#FDE803',
                    }
                }
            }}
        />
    </ShowAllExamsStack.Navigator>
)

export default ShowAllExamsStackScreen;
