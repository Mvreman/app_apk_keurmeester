import { createStackNavigator } from "@react-navigation/stack";
import {EditExam} from "../screens";
import React from "react";
import MenuBar from "../source/elements/menuBar";

/**
 * Stack for showing the edit exam screen
 *
 * @type {import("@react-navigation/native").TypedNavigator<Record<string, object | undefined>, StackNavigationState, StackNavigationOptions, StackNavigationEventMap, typeof StackNavigator>}
 */
const EditExamnavigator = createStackNavigator();
const EditExamStackScreen = () => (
    <EditExamnavigator.Navigator>
        <EditExamnavigator.Screen
            name={"EditExam"}
            component={EditExam}
            options={ ({ navigation }) => {
                return {
                    headerTitle: () => <MenuBar navigation={navigation} title={'Edit exam'} />,
                    headerStyle: {
                        backgroundColor: '#FDE803',
                    }
                }
            }}
        />
    </EditExamnavigator.Navigator>
);

export default EditExamStackScreen;
