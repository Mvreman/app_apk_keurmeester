import { createStackNavigator } from "@react-navigation/stack";
import { Login, Logout } from "../screens";
import React from "react";

const logoutStack = createStackNavigator();
const LogoutStackScreen = () => (
    <logoutStack.Navigator>
        <logoutStack.Screen
            name={"Logout"}
            component={ Logout }
            options={ ({ navigation }) => {
                return {
                    headerShown: false,
                }
            }}
        />
        <logoutStack.Screen
            name={"Login"}
            component={Login}
            options={ ({ navigation }) => {
                return {
                    headerShown: false,
                }
            }}
        />
    </logoutStack.Navigator>
)

export default LogoutStackScreen;
