import Home from './homeStack';
import Exams from './examStack';
import EditExam from './editExamStack';
import Profile from './profileStack';
import Login from './loginStack';
import Logout from './logoutStack';
import Personenauto from './personenautoStack';
import FinishPracticeExam from './practiceExam/finishStack'
import ShowAllExams from "./ShowAllExamsStack";
import ShowArticle from "./RegelgevingShowStack";
import SideArticle from "./RegelgevingSideStack"
import Berichten from './messageStack';
import ExamsResult from "./ExamsResultStack";

//Practice Exam Imports
import CreatePracticeExam from './practiceExam/createStack';
import CasePracticeExam from './practiceExam/caseStack';
import PracticeQuestions from './practiceExam/practiceQuestionStack';
import PracticeExamResult from './practiceExam/resultStack';
import ShowPracticeExam from './practiceExam/showStack';

export {
    Home,
    Exams,
    EditExam,
    Login,
    Logout,
    Profile,
    Personenauto,
    ShowAllExams,
    ShowArticle,
    SideArticle,
    Berichten,
    ExamsResult,

    // Practice Exam Exports
    CreatePracticeExam,
    CasePracticeExam,
    PracticeQuestions,
    FinishPracticeExam,
    PracticeExamResult,
    ShowPracticeExam,
}
