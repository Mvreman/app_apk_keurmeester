import { createStackNavigator } from "@react-navigation/stack";
import { Messages } from "../screens";
import React from "react";
import { MenuBar } from "../source/elements/index";

/**
 * Stack for showing of all messages screen
 *
 * @type {import("@react-navigation/native").TypedNavigator<Record<string, object | undefined>, StackNavigationState, StackNavigationOptions, StackNavigationEventMap, typeof StackNavigator>}
 */
const MessageStack = createStackNavigator();
const MessageStackScreen = () => (
    <MessageStack.Navigator>
        <MessageStack.Screen
            name={"Berichten"}
            component={Messages}
            options={ ({ navigation }) => {
                return {
                    headerTitle: () => <MenuBar navigation={navigation} title={'Berichten'} />,
                    headerStyle: {
                        backgroundColor: '#FDE803',
                    }
                }
            }}
        />
    </MessageStack.Navigator>
)

export default MessageStackScreen;
