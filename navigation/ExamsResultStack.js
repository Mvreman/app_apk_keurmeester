import { createStackNavigator } from "@react-navigation/stack";
import {ExamsResult} from "../screens";
import React from "react";
import MenuBar from "../source/elements/menuBar";

/**
 * Stack for showing the examsresult screen
 *
 * @type {import("@react-navigation/native").TypedNavigator<Record<string, object | undefined>, StackNavigationState, StackNavigationOptions, StackNavigationEventMap, typeof StackNavigator>}
 */
const ExamsResultStack = createStackNavigator();
const ExamsResultStackScreen = () => (
    <ExamsResultStack.Navigator>
        <ExamsResultStack.Screen
            name={"ExamsResult"}
            component={ExamsResult}
            options={ ({ navigation }) => {
                return {
                    headerTitle: () => <MenuBar navigation={navigation} title={'Mijn Resultaten'} />,
                    headerStyle: {
                        backgroundColor: '#FDE803',
                    }
                }
            }}
        />
    </ExamsResultStack.Navigator>
)

export default ExamsResultStackScreen;
