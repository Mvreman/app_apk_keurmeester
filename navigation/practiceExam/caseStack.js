import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import MenuBar from "../../source/elements/menuBar";
import { CasePracticeExam } from "../../screens"

/**
 * Stack for the case screen of practice exam
 *
 * @type {import("@react-navigation/native").TypedNavigator<Record<string, object | undefined>, StackNavigationState, StackNavigationOptions, StackNavigationEventMap, typeof StackNavigator>}
 */
const CaseStack = createStackNavigator();
const CaseStackScreen = () => (
    <CaseStack.Navigator>
        <CaseStack.Screen
            name={"caseScreen"}
            component={CasePracticeExam}
            options={ ({ navigation }) => {
                return {
                    headerTitle: () => <MenuBar navigation={navigation} title={'Casus'}/>,
                    headerStyle: {
                        backgroundColor: '#FDE803',
                    }
                }
            }}
        />
    </CaseStack.Navigator>
);

export default CaseStackScreen;
