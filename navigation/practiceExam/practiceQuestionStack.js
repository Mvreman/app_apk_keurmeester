import { createStackNavigator } from "@react-navigation/stack";
import {PracticeQuestions} from "../../screens";
import React from "react";
import MenuBar from "../../source/elements/menuBar";

/**
 * Stack for the practice questions of practice exam
 *
 * @type {import("@react-navigation/native").TypedNavigator<Record<string, object | undefined>, StackNavigationState, StackNavigationOptions, StackNavigationEventMap, typeof StackNavigator>}
 */
const PracticeQuestionsStack = createStackNavigator();
const PracticeQuestionsStackScreen = () => (
    <PracticeQuestionsStack.Navigator>
        <PracticeQuestionsStack.Screen
            name={"practiceQuestionsScreen"}
            component={PracticeQuestions}
            options={ ({ navigation }) => {
                return {
                    headerTitle: () => <MenuBar navigation={navigation} title={'Oefenvragen'}/>,
                    headerStyle: {
                        backgroundColor: '#FDE803',
                    }
                }
            }}
        />
    </PracticeQuestionsStack.Navigator>
);

export default PracticeQuestionsStackScreen;
