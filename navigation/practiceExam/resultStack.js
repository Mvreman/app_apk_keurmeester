import { createStackNavigator } from "@react-navigation/stack";
import {PracticeExamResult} from "../../screens";
import React from "react";
import MenuBar from "../../source/elements/menuBar";

/**
 * Stack for the result screen of practice exam
 *
 * @type {import("@react-navigation/native").TypedNavigator<Record<string, object | undefined>, StackNavigationState, StackNavigationOptions, StackNavigationEventMap, typeof StackNavigator>}
 */
const ResultStack = createStackNavigator();
const ResultStackScreen = () => (
    <ResultStack.Navigator>
        <ResultStack.Screen
            name={"resultScreen"}
            component={PracticeExamResult}
            options={ ({ navigation }) => {
                return {
                    headerTitle: () => <MenuBar navigation={navigation} title={'Einde van het oefenexamen'}/>,
                    headerStyle: {
                        backgroundColor: '#FDE803',
                    }
                }
            }}
        />
    </ResultStack.Navigator>
);

export default ResultStackScreen;