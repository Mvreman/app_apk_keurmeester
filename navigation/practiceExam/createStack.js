import { createStackNavigator } from "@react-navigation/stack";
import {CreatePracticeExam} from "../../screens";
import React from "react";
import MenuBar from "../../source/elements/menuBar";

/**
 * Stack for the create screen of practice exam
 *
 * @type {import("@react-navigation/native").TypedNavigator<Record<string, object | undefined>, StackNavigationState, StackNavigationOptions, StackNavigationEventMap, typeof StackNavigator>}
 */
const CreateStack = createStackNavigator();
const CreateStackScreen = () => (
    <CreateStack.Navigator>
        <CreateStack.Screen
            name={"createScreen"}
            component={CreatePracticeExam}
            options={ ({ navigation }) => {
                return {
                    headerTitle: () => <MenuBar navigation={navigation} title={'Oefenvragen'}/>,
                    headerStyle: {
                        backgroundColor: '#FDE803',
                    }
                }
            }}
        />
    </CreateStack.Navigator>
);

export default CreateStackScreen;
