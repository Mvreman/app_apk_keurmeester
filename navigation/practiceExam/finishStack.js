import { createStackNavigator } from "@react-navigation/stack";
import {FinishPracticeExam} from "../../screens";
import React from "react";
import MenuBar from "../../source/elements/menuBar";

/**
 * Stack for the finish screen of practice exam
 *
 * @type {import("@react-navigation/native").TypedNavigator<Record<string, object | undefined>, StackNavigationState, StackNavigationOptions, StackNavigationEventMap, typeof StackNavigator>}
 */
const FinishStack = createStackNavigator();
const FinishStackScreen = () => (
    <FinishStack.Navigator>
        <FinishStack.Screen
            name={"finishScreen"}
            component={FinishPracticeExam}
            options={ ({ navigation }) => {
                return {
                    headerTitle: () => <MenuBar navigation={navigation} title={'Afsluiten Oefenexamen'}/>,
                    headerStyle: {
                        backgroundColor: '#FDE803',
                    }
                }
            }}
        />
    </FinishStack.Navigator>
);

export default FinishStackScreen;
