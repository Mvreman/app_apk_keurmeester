import { createStackNavigator } from "@react-navigation/stack";
import {ShowPracticeExam} from "../../screens";
import React from "react";
import MenuBar from "../../source/elements/menuBar";

/**
 * Stack for the show screen of practice exam
 *
 * @type {import("@react-navigation/native").TypedNavigator<Record<string, object | undefined>, StackNavigationState, StackNavigationOptions, StackNavigationEventMap, typeof StackNavigator>}
 */
const ShowStack = createStackNavigator();
const ShowStackScreen = () => (
    <ShowStack.Navigator>
        <ShowStack.Screen
            name={"showScreen"}
            component={ShowPracticeExam}
            options={ ({ navigation }) => {
                return {
                    headerTitle: () => <MenuBar navigation={navigation} title={'Inzien'}/>,
                    headerStyle: {
                        backgroundColor: '#FDE803',
                    }
                }
            }}
        />
    </ShowStack.Navigator>
);

export default ShowStackScreen;
