import { createStackNavigator } from "@react-navigation/stack";
import {Personenauto} from "../screens";
import React from "react";
import MenuBar from "../source/elements/menuBar";

/**
 * Stack for showing of personenauto screen
 *
 * @type {import("@react-navigation/native").TypedNavigator<Record<string, object | undefined>, StackNavigationState, StackNavigationOptions, StackNavigationEventMap, typeof StackNavigator>}
 */
const PersonenautoStack = createStackNavigator();
const PersonenautoStackScreen = () => (
    <PersonenautoStack.Navigator>
        <PersonenautoStack.Screen
            name={"Personenauto"}
            component={Personenauto}
            options={ ({ navigation }) => {
                return {
                    headerTitle: () => <MenuBar navigation={navigation} title={'Personenauto'} />,
                    headerStyle: {
                        backgroundColor: '#FDE803',
                    }
                }
            }}
        />
    </PersonenautoStack.Navigator>
);

export default PersonenautoStackScreen
