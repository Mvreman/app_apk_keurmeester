import { createStackNavigator } from "@react-navigation/stack";
import { Home } from "../screens";
import React from "react";
import { MenuBar } from "../source/elements/index";

/**
 * Stack for showing of home screen
 *
 * @type {import("@react-navigation/native").TypedNavigator<Record<string, object | undefined>, StackNavigationState, StackNavigationOptions, StackNavigationEventMap, typeof StackNavigator>}
 */
const HomeStack = createStackNavigator();
const HomeStackScreen = () => (
    <HomeStack.Navigator>
        <HomeStack.Screen
            name={"Home"}
            component={Home}
            options={ ({ navigation }) => {
                return {
                    headerTitle: () => <MenuBar navigation={navigation} title={'Home'} />,
                    headerStyle: {
                        backgroundColor: '#FDE803',
                    },

                }
            }}
        />
    </HomeStack.Navigator>
)

export default HomeStackScreen;
