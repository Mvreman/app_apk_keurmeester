import { createStackNavigator } from "@react-navigation/stack";
import {RegelgevingSide} from "../screens";
import React from "react";
import MenuBar from "../source/elements/menuBar";

/**
 * Stack for the screen between article and personenauto
 *
 * @type {import("@react-navigation/native").TypedNavigator<Record<string, object | undefined>, StackNavigationState, StackNavigationOptions, StackNavigationEventMap, typeof StackNavigator>}
 */
const RegelgevingSideStack = createStackNavigator();
const RegelgevingSideStackScreen = () => (
    <RegelgevingSideStack.Navigator>
        <RegelgevingSideStack.Screen
            name={"RegelgevingSide"}
            component={RegelgevingSide}
            options={ ({ navigation }) => {
                return {
                    headerTitle: () => <MenuBar navigation={navigation} title={'RegelgevingShow'} />,
                    headerStyle: {
                        backgroundColor: '#FDE803',
                    }
                }
            }}
        />
    </RegelgevingSideStack.Navigator>
);
export default RegelgevingSideStackScreen;
