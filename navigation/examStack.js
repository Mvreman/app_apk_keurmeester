import { createStackNavigator } from "@react-navigation/stack";
import { AddExamStepOne, AddExamStepTwo, AddExamStepThree, AddExamStepFour } from "../screens";
import React from "react";
import MenuBar from "../source/elements/menuBar";

/**
 * Stack for showing the screen of a specific exam
 *
 * @type {import("@react-navigation/native").TypedNavigator<Record<string, object | undefined>, StackNavigationState, StackNavigationOptions, StackNavigationEventMap, typeof StackNavigator>}
 */
const examsStack = createStackNavigator();
const examsStackScreen = () => (
    <examsStack.Navigator>
        <examsStack.Screen
            name={"BookExamStep01"}
            component={ AddExamStepOne }
            options={({ navigation }) => {
                return {
                    headerTitle: () => <MenuBar navigation={navigation} title={'Examen boeken'} />,
                    headerStyle: {
                        backgroundColor: '#FDE803',
                    }
                }
            }}
        />
        <examsStack.Screen
            name={"BookExamStep02"}
            component={ AddExamStepTwo }
            options={({ navigation }) => {
                return {
                    headerTitle: () => <MenuBar navigation={navigation} title={'Examen boeken'} />,
                    headerStyle: {
                        backgroundColor: '#FDE803',
                    }
                }
            }}
        />
        <examsStack.Screen
            name={"BookExamStep03"}
            component={ AddExamStepThree }
            options={({ navigation }) => {
                return {
                    headerTitle: () => <MenuBar navigation={navigation} title={'Examen boeken'} />,
                    headerStyle: {
                        backgroundColor: '#FDE803',
                    }
                }
            }}
        />
        <examsStack.Screen
            name={"BookExamStep04"}
            component={ AddExamStepFour }
            options={({ navigation }) => {
                return {
                    headerTitle: () => <MenuBar navigation={navigation} title={'Examen boeken'}/>,
                    headerStyle: {
                        backgroundColor: '#FDE803',
                    }
                }
            }}
        />
    </examsStack.Navigator>
);

export default examsStackScreen;
