import { createStackNavigator } from "@react-navigation/stack";
import {RegelgevingShow} from "../screens";
import React from "react";
import MenuBar from "../source/elements/menuBar";

/**
 * Stack for showing all law articles screen
 *
 * @type {import("@react-navigation/native").TypedNavigator<Record<string, object | undefined>, StackNavigationState, StackNavigationOptions, StackNavigationEventMap, typeof StackNavigator>}
 */
const RegelgevingShowStack = createStackNavigator();
const RegelgevingShowStackScreen = () => (
    <RegelgevingShowStack.Navigator>
        <RegelgevingShowStack.Screen
            name={"RegelgevingShow"}
            component={RegelgevingShow}
            options={ ({ navigation }) => {
                return {
                    headerTitle: () => <MenuBar navigation={navigation} title={'RegelgevingShow'} />,
                    headerStyle: {
                        backgroundColor: '#FDE803',
                    }
                }
            }}
        />
    </RegelgevingShowStack.Navigator>
);

export default RegelgevingShowStackScreen;
