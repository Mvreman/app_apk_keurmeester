import { createStackNavigator } from "@react-navigation/stack";
import {Login} from "../screens";
import React from "react";

/**
 * Stack for showing the login screen
 *
 * @type {import("@react-navigation/native").TypedNavigator<Record<string, object | undefined>, StackNavigationState, StackNavigationOptions, StackNavigationEventMap, typeof StackNavigator>}
 */
const loginStack = createStackNavigator();
const LoginStackScreen = () => (
    <loginStack.Navigator>
        <loginStack.Screen
            name={"Login"}
            component={Login}
            options={ ({ navigation }) => {
                return {
                    headerShown: false,
                }
            }}
        />
    </loginStack.Navigator>
)

export default LoginStackScreen;
