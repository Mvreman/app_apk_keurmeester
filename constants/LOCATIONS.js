export default {
    HELMOND: 'Helmond',
    DORDRECHT: 'Dordrecht',
    NIEUWEGEIN: 'Niuewegein',
    ZWOLLE: 'Zwolle',
    DROPDOWN: [
        { value: 'Helmond', label: 'Helmond' },
        { value: 'Dordrecht', label: 'Dordrecht' },
        { value: 'Nieuwegein', label: 'Nieuwegein' },
        { value: 'Zwolle', label: 'Zwolle' },
    ]
}