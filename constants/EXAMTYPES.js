export default {
    AL: 'al',
    AZ: 'az',
    DROPDOWN: [
        { value: 'APK Lichte voertuigen', label: 'APK lichte voertuigen' },
        { value: 'APK Bevoegheidsverlenging Licht', label: 'APK Bevoegheidsverlenging Licht' },
        { value: 'APK Bevoegheidsverlenging Zwaar', label: 'APK Bevoegheidsverlenging Zwaar' },
        { value: 'APK Zware voertuigen', label: 'APK Zware voertuigen' },
    ],
    lightweight: [
        { label: "APK Bevoegheidsverlenging Licht", value: "APK Bevoegheidsverlenging Licht" },
        { label: "APK Lichte voertuigen", value: "APK Lichte voertuigen" },
    ],
    heavyweight: [
        { label: "APK Bevoegheidsverlenging Zwaar", value: "APK Bevoegheidsverlenging Zwaar" },
        { label: "APK Zware voertuigen", value: "APK Zware voertuigen" },
    ]
}