export default {
    LICHT: 'Licht',
    ZWAAR: 'Zwaar',
    PERSONENAUTOS: 'Personenauto’s',
    BEDRIJFAUTOS_LICHT: 'Bedrijfsauto’s licht',
    BEDRIJFSAUTOS_ZWAAR: 'Bedrijfsauto’s-Zwaar',
    DRIEWIELIGE_MOTORRIJTUIGEN: 'Driewielige motorrijtuigen',
    AANHANGWAGENS: 'Aanhangwagens',
    OVERIGE_REGELGEVING: 'Overige regelgeving',
    
    DROPDOWN: [
        { value: 'Personenauto’s', label: 'Personenauto’s' },
        { value: 'Bedrijfsauto’s licht', label: 'Bedrijfsauto’s licht' },
        { value: 'Bedrijfsauto’s-Zwaar', label: 'Bedrijfsauto’s-Zwaar' },
        { value: 'Driewielige motorrijtuigen', label: 'Driewielige motorrijtuigen' },
        { value: 'Aanhangwagens', label: 'Aanhangwagens' },
        { value: 'Overige regelgeving', label: 'Overige regelgeving' },
    ]
}