import React from 'react';
import {useFonts} from '@use-expo/font';
import {AppLoading} from "expo";
import {NavigationContainer} from "@react-navigation/native";
import {createDrawerNavigator} from "@react-navigation/drawer";
import {Provider} from "react-redux";
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import store from "./redux/store";
import {
    Home,
    Personenauto,
    ShowAllExams,
    EditExam,
    Profile,
    Login,
    Exams,
    Berichten,
    ExamsResult,
    ShowArticle,
    SideArticle,
    Logout,

    // Practice exam imports
    CreatePracticeExam,
    CasePracticeExam,
    PracticeQuestions,
    FinishPracticeExam,
    PracticeExamResult,
    ShowPracticeExam
} from './navigation/index';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { DrawerContent } from './screens/DrawerContent'

const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();

/**
 * Tab navigator for practice exams
 *
 * @returns {*}
 * @constructor
 */
function PracticeExam() {
    return (
        <Tab.Navigator>
            <Tab.Screen options={{tabBarVisible: false}} name='CreatePracticeExam' component={CreatePracticeExam}/>
            <Tab.Screen options={{tabBarVisible: false}} name='CasePracticeExam' component={CasePracticeExam}/>
            <Tab.Screen options={{tabBarVisible: false}} name='PracticeQuestions' component={PracticeQuestions}/>
            <Tab.Screen options={{tabBarVisible: false}} name='FinishPracticeExam' component={FinishPracticeExam}/>
            <Tab.Screen options={{tabBarVisible: false}} name='PracticeExamResult' component={PracticeExamResult}/>
            <Tab.Screen options={{tabBarVisible: false}} name="ShowPracticeExam" component={ShowPracticeExam}/>
        </Tab.Navigator>
    )
}

function Regelgeving() {

    return (
        <Tab.Navigator >
            <Tab.Screen options={{ tabBarVisible: false}} name='Personenauto' component={Personenauto} />
            <Tab.Screen options={{ tabBarVisible: false}} name='RegelgevingShow' component={ShowArticle}/>
            <Tab.Screen options={{ tabBarVisible: false}} name='RegelgevingSide' component={SideArticle}/>
        </Tab.Navigator >
    )
}

function MyExams() {

    return (
        <Tab.Navigator >
            <Tab.Screen options={{ tabBarVisible: false}} name= 'Examens' component={ShowAllExams} />
            <Tab.Screen options={{ tabBarVisible: false}} name='Examen toevoegen' component={Exams}/>
            <Tab.Screen options={{ tabBarVisible: false}} name='EditExam' component={EditExam}/>
        </Tab.Navigator >
    )
}


export default function App() {
    /**
     * Loading in fonts
     */
    const [fontsLoaded] = useFonts({
        'Roboto': require('./source/fonts/Roboto-Regular.ttf'),
        'Roboto-light': require('./source/fonts/Roboto-Light.ttf'),
        'Roboto-medium': require('./source/fonts/Roboto-Medium.ttf'),
        'Roboto-bold': require('./source/fonts/Roboto-Bold.ttf'),
        'Roboto-slab': require('./source/fonts/RobotoSlab-Regular.ttf'),
        'Roboto-slab-bold': require('./source/fonts/RobotoSlab-Bold.ttf'),
    });

    if (!fontsLoaded) {
        return <AppLoading/>
    } else {
        return (
            <Provider store={store}>
                <NavigationContainer>
                    <Drawer.Navigator
                        drawerStyle={{
                            width: "80%",
                        }}
                        initialRouteName='Login'
                        drawerContentOptions={{
                            inactiveTintColor: '#ffffff',
                            inactiveBackgroundColor: '#404040',
                            activeTintColor: '#ffffff',
                            activeBackgroundColor: '#151512'
                        }}
                        drawerContent={ props => <DrawerContent {...props} />}>
                        <Drawer.Screen name='Home' component={Home} options={{
                                         drawerIcon: config => <Icon color={"#ffffff"} size={20} name={'home'}/>
                                       }}/>
                        <Drawer.Screen name='Regelgeving' component={Regelgeving} options={{
                                           drawerIcon: config => <Icon color={"#ffffff"} size={20} name={'gavel'}/>
                                       }}/>
                        <Drawer.Screen name='Mijn Examens' component={MyExams} options={{
                                           drawerIcon: config => <Icon color={"#ffffff"} size={20} name={'assignment-turned-in'}/>
                                       }}/>
                        <Drawer.Screen name='Mijn Resultaten' component={ExamsResult} options={{
                                           drawerIcon: config => <Icon color={"#ffffff"} size={20} name={'assessment'}/>
                                       }}/>
                        <Drawer.Screen name='Berichten' component={Berichten} options={{
                                           drawerIcon: config => <Icon color={"#ffffff"} size={20} name={'comment'}/>
                                       }}/>
                        <Drawer.Screen name='Oefenvragen' component={PracticeExam} options={{
                                           drawerIcon: config => <Icon color={"#ffffff"} size={20} name={'playlist-add-check'}/>
                                       }}/>
                        <Drawer.Screen name='Instellingen' component={Profile} options={{
                                           drawerIcon: config => <Icon color={"#ffffff"} size={20} name={'settings'}/>
                                       }}/>
                        <Drawer.Screen name='Logout' component={Logout} options={{
                            drawerIcon: config => <Icon color={"#ffffff"} size={20} name={'exit-to-app'}/>
                        }}/>
                        <Drawer.Screen name='Login' component={Login} options={{
                            drawerLabel: () => null,
                            gestureEnabled: false,
                        }}
                        />
                    </Drawer.Navigator>
                </NavigationContainer>
            </Provider>
        );
    }
}
